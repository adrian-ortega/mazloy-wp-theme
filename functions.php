<?php

require_once __DIR__ . '/inc/theme.php';

// Add the loading animation to the footer
mazloy()->action('mazloy.before_body', function() {
	if(mazloy_enabled('loader'))
		mazloy_partial('loader');
});

// Add the back-to-top button
mazloy()->action('mazloy.after_body', function() {
	if(mazloy_enabled('back_to_top'))
		echo '<a name="top"></a>';
});

mazloy()->action('mazloy.before_body', function() {
	if(mazloy_enabled('back_to_top'))
		echo '<a href="#top" class="back-to-top"><span class="mdi mdi-chevron-up"></span></a>';
});

wm_start();

//dd(mazloy('quick_metaboxes'));