<?php mazloy_partial( 'loop' ); ?>
<div class="pagination">
    <div class="row">
        <div class="col-xs-6"><?php previous_posts_link(__('Newer Posts', mazloy('textdomain'))) ?></div>
        <div class="col-xs-6 text-right"><?php next_posts_link(__('Older Posts', mazloy('textdomain'))) ?></div>
    </div>
</div>