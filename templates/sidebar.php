<?php if (is_active_sidebar('sidebar-1')): ?>
    <?php
    $sidebar_classes = ['widget-area', ' widget-sidebar-area'];

    if(is_single())
        $sidebar_classes[] = 'widget-sidebar-area-single';

    if(is_archive())
        $sidebar_classes[] = 'widget-sidebar-area-archive';

    if(is_search())
        $sidebar_classes[] = 'widget-sidebar-area-search';

    $sidebar_classes = join(' ', $sidebar_classes);
    ?>

    <div class="<?php echo $sidebar_classes ?>" role="complementary">
        <?php dynamic_sidebar('sidebar-1'); ?>
    </div>
<?php endif;