<?php
/**
 * Template Name: UI Kit
 */
$options = '';
for($i = 1; $i < 11; $i++) {
    $options .= '<option value="' . $i . '">Dropdown Option ' . $i . '</option>';
}
$sets = [
	[
		'heading'     => 'Typography',
		'sub_heading' => 'Headers',
		'examples'    => [
			'H1' => '<h1>The Quick Brown Fox</h1>',
			'H2' => '<h2>The Quick Brown Fox Jumps over the Lazy Dog</h2>',
			'H3' => '<h3>The Quick Brown Fox Jumps over the Lazy Dog</h3>',
			'H4' => '<h4>The Quick Brown Fox Jumps over the Lazy Dog</h4>',
		]
	],
	[
		'sub_heading' => 'Body and Links',
		'examples'    => [
			'Body'       => '<p>"The quick brown fox jumps over the lazy dog" is an English-language pangram—a sentence that contains all of the letters of the alphabet. It is commonly used for touch-typing practice, testing typewriters and computer keyboards, displaying examples of fonts, and other applications involving text where the use of all letters in the alphabet is desired. Owing to its brevity and coherence, it has become widely known.</p>',
			'Body Small' => '<p><small>"The quick brown fox jumps over the lazy dog" is an English-language pangram—a sentence that contains all of the letters of the alphabet. It is commonly used for touch-typing practice, testing typewriters and computer keyboards, displaying examples of fonts, and other applications involving text where the use of all letters in the alphabet is desired. Owing to its brevity and coherence, it has become widely known.</small></p>',
			'Link'       => [
				'<a href="javascript:void(0)">This is a link</a>',
				'<a href="javascript:void(0)" class="force-hover">This is a Hovered Link</a>',
				'<a href="javascript:void(0)" class="force-active">This is an Active Link</a>'
			]
		]
	],
	[
		'heading'     => 'UI Elements',
		'sub_heading' => 'Buttons',
		'examples'    => [
			'Base State'        => [
				'<a href="javascript:void(0)" class="button">Learn More</a>',
				'<a href="javascript:void(0)" class="button button--outline">Learn More</a>',
				'<a href="javascript:void(0)" class="button button--secondary">Learn More</a>',
				'<a href="javascript:void(0)" class="button button--outline button--secondary">Learn More</a>',
			],
			'Hover/Focus State' => [
				'<a href="javascript:void(0)" class="button force-hover">Learn More</a>',
				'<a href="javascript:void(0)" class="button force-hover button--outline">Learn More</a>',
				'<a href="javascript:void(0)" class="button force-hover button--secondary">Learn More</a>',
				'<a href="javascript:void(0)" class="button force-hover button--outline button--secondary">Learn More</a>',
			],
			'Active State'      => [
				'<a href="javascript:void(0)" class="button force-active">Learn More</a>',
				'<a href="javascript:void(0)" class="button force-active button--outline">Learn More</a>',
				'<a href="javascript:void(0)" class="button force-active button--secondary">Learn More</a>',
				'<a href="javascript:void(0)" class="button force-active button--outline button--secondary">Learn More</a>',
			]
		]
	],
	[
		'sub_heading' => 'Inputs',
		'examples'    => [
			'Base Text Input' => '<label for="text_base">Optional Label</label><input type="text" id="text_base" placeholder="Placeholder Text">',
			'Hover State' => '<label class="force-hover" for="text_hover">Optional Label</label><input type="text" id="text_hover" class="force-hover" placeholder="Placeholder Text">',
			'Focus State' => '<label class="force-focus" for="text_focus">Optional Label</label><input type="text" id="text_focus" class="force-focus" placeholder="Placeholder Text">',
            'Base Select' => '<label for="select_base">Optional Label</label><select id="select_base">'.$options.'</select>'
        ]
    ]
]
?>

<div class="wrapper ui-kit">
	<?php foreach ( $sets as $set ): ?>
		<?php if ( isset( $set['heading'] ) ): ?>
            <h2 class="ui-kit__heading"><?php echo $set['heading'] ?></h2>
		<?php endif ?>
		<?php if ( isset( $set['sub_heading'] ) ): ?>
            <h3 class="ui-kit__sub-heading"><span><?php echo $set['sub_heading'] ?></span></h3>
		<?php endif ?>
		<?php foreach ( $set['examples'] as $label => $example ): ?>
            <div class="ui-kit__row">
                <div class="row">
                    <div class="col-xs col-md-2"><span class="ui-kit__label"><?php echo $label ?></span></div>
                    <div class="col-xs col-md-10">
						<?php if ( is_array( $example ) ): ?>
                            <div class="row">
								<?php foreach ( $example as $exampleCol ): ?>
                                    <div class="col-xs col-md-<?php echo 12 / count( $example ) ?>">
										<?php echo $exampleCol ?>
                                    </div>
								<?php endforeach ?>
                            </div>
						<?php else: echo $example; endif; ?>
                    </div>
                </div>
            </div>
		<?php endforeach ?>
	<?php endforeach ?>
</div>