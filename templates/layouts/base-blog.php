<!doctype html>
<html <?php language_attributes(); ?>>
<?php mazloy_partial('layout.head'); ?>
<body <?php body_class(); ?>>
<?php do_action('mazloy.after_body') ?>
<div role="document">
	<?php do_action('get_header') ?>
	<?php mazloy_partial('header') ?>
	<?php is_front_page() ? mazloy_partial('sliders.slider', 'front-page') : null ?>
	<main<?php mazloy_content_classes() ?>>
        <?php if($page_for_posts = get_option('page_for_posts')):?>
        <?php mazloy_partial('layout.page-header', [
            '_id' => $page_for_posts,
            'sub_title' => is_archive() ? get_the_archive_title() : ''
        ]) ?>
        <?php endif ?>
        <div class="container">
            <div class="row">
                <div class="col-xs col-md-7">
                    <?php mazloy()->template()->main(); ?>
                </div>
                <div class="col-xs col-md-3 col-md-offset-2">
                    <?php get_sidebar( 'right' ) ?>
                </div>
            </div>
        </div>
	</main>
	<?php do_action('get_footer') ?>
	<?php mazloy_partial('footer') ?>
</div>
<?php do_action('mazloy.before_body') ?>
<?php wp_footer() ?>
<?php do_action('mazloy.absolute_footer') ?>
</body>
</html>
