<!doctype html>
<html <?php language_attributes(); ?>>
<?php mazloy_partial('layout.head'); ?>
<body <?php body_class(); ?>>
<?php do_action('mazloy.after_body') ?>
<div role="document">
    <?php do_action('get_header') ?>
    <?php mazloy_partial('header') ?>
    <?php mazloy_partial('sliders.slider', is_front_page() ? 'front-page' : get_post_type()) ?>
    <main<?php mazloy_content_classes() ?>>
        <?php mazloy()->template()->main(); ?>
    </main>
    <?php do_action('get_footer') ?>
    <?php mazloy_partial('footer') ?>
</div>
<?php do_action('mazloy.before_body') ?>
<?php wp_footer() ?>
<?php do_action('mazloy.absolute_footer') ?>
</body>
</html>
