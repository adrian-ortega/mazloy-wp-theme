<?php if(($slides = mazloy_get_slides('mazloy-home-slide')) && count($slides) > 0) : ?>
    <div id="front-page-slider" class="slider slider--front-page">
        <?php foreach($slides as $slide): ?>
            <article class="slide slide--<?php echo $slide->get('font_color') ?>">
                <?php if($slide->get('type') == 'video'):
                    printf('<div class="slide__video">%s</div>', $slide->video_shortcode());
                else:
                    printf('<div%s></div>', $slide->slideAttributes());
                endif;
                if($slide->has_content()):?>
                    <div class="slide__content">
                        <div class="container">
                            <div <?php $slide->rowAttributes() ?>>
                                <div class="col-xs">
                                    <?php if($slide->has_content()):?>
                                    <div class="slide__caption<?php echo $slide->get('caption_background') ? ' slide__caption--has-background' : '' ?>">
                                        <?php if($slide->has('heading')):
                                            printf('<h2 class="slide__heading">%s</h2>', $slide->get('heading'));
                                        endif; if($slide->has('caption')):
                                            echo $slide->get('caption');
                                        endif ?>
                                    </div>
                                    <?php endif; if($slide->has('button1.url') || $slide->has('button2.url')):?>
                                    <div class="slide__button">
                                        <?php $slide->button(1) ?>
                                        <?php $slide->button(2) ?>
                                    </div>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </article>
	    <?php endforeach;?>
    </div>
<?php endif;