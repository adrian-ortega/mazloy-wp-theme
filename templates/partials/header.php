<header class="header header--main">
    <?php if(mazloy_enabled('header_utility_menu_enable')) : ?>
    <div class="header__utility">
        <div class="container">
            <div class="row between-xs">
                <div class="col-xs col-md-3">
                    <?php mazloy_partial('social-links', 'header') ?>
                </div>
                <div class="col-xs">
                    <?php mazloy_utility_menu() ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif ?>
    <div class="header__main">
        <div class="container">
            <div class="row between-xs">
                <div class="col-xs col-md-3">
                    <div class="logo-wrapper" itemscope itemtype="http://schema.org/Organization">
                        <a id="logo" href="<?php echo get_home_url() ?>">
                            <img src="<?php mazloy_image( 'logo-448x62.png' ) ?>" alt="<?php echo bloginfo('name') ?>" itemprop="logo">
                        </a>
                    </div>
                </div>
                <div class="col-xs">
                    <?php mazloy_main_menu() ?>
                </div>
            </div>
        </div>
    </div>
</header>