<article id="post-<?php the_ID() ?>" <?php post_class(['entry', 'entry--preview']) ?> itemscope itemtype="http://schema.org/Article">
    <?php mazloy_partial('post.header') ?>
    <?php mazloy_partial('post.thumbnail') ?>
    <section class="entry__content">
        <?php the_excerpt() ?>
        <p class="read-more">
            <a href="" class="button button--small">
                <?php _e('Continue Reading', mazloy('textdomain')) ?>
            </a>
        </p>
    </section>

    <?php if(mazloy_is_categorized()):?>
    <footer class="entry__footer">
        <div class="entry__meta">
            <?php mazloy_partial('post.meta.categories') ?>
        </div>
    </footer>
    <?php endif ?>
</article>