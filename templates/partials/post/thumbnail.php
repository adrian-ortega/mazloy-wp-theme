<?php
if(!has_post_thumbnail())
	return null;

$thumbnail_size = isset($thumbnail_size) ? $thumbnail_size : 'mazloy-preview';
$aspect_ratio = isset($aspect_ratio) ? $aspect_ratio : true;
?>
<div class="entry__thumbnail-wrapper<?php echo has_post_thumbnail() ? ' entry-thumbnail-wrapper-none': ''?>">
	<?php mazloy_post_thumbnail($thumbnail_size, true, $aspect_ratio) ?>
</div>