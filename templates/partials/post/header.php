<header class="entry__header<?php echo has_post_thumbnail() ? '' : ' entry__header--no-thumbnail'?>">
    <?php edit_post_link(sprintf(
        '<i class="fa fa-pencil"></i><span>%s</span>',
        __('Edit', mazloy('textdomain'))
    )) ?>

    <h1 class="entry__title">
        <?php printf('%s%s%s',
            is_single() ? '' : sprintf('<a href="%s" title="%s">', get_the_permalink(), get_the_title()),
            sprintf('<span itemprop="name">%s</span>', get_the_title()),
            is_single() ? '' : '</a>'
        ) ?>
    </h1>
    <div class="entry__meta">
		<?php mazloy_partial('post.meta.author') ?>
		<?php mazloy_partial('post.meta.date') ?>
		<?php mazloy_partial('post.meta.comments') ?>
    </div>
</header>