<?php if($tags_list = get_the_tag_list('', '')) :?>
<div class="entry__meta-item entry__meta-item--entry-tags">
    <i class="fa fa-tags"></i>
    <?php echo $tags_list ?>
</div>
<?php endif ?>