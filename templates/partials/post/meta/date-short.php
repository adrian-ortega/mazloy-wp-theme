<?php if (in_array(get_post_type(), array('post', 'attachment'))): ?>
<span class="entry__meta-item entry__meta-item--date-short">
    <a href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark">
        <time datetime="<?php echo esc_attr(get_the_date('c')) ?>">
            <span class="entry-date-day"><?php echo get_the_date('d') ?></span>
            <span class="entry-date-month"><?php echo get_the_date('M') ?></span>
        </time>
    </a>
</span>
<?php endif ?>