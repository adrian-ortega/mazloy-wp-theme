<?php if (in_array(get_post_type(), array('post', 'attachment'))): ?>
<span class="entry__meta-item entry__meta-item--date">
    <span class="entry__meta-label entry__meta-label--date"><?php _e('on', mazloy('textdomain')) ?></span>
    <a href="<?php echo esc_url(get_permalink()) ?>" rel="bookmark">
        <time datetime="<?php echo esc_attr(get_the_date('c')) ?>">
            <?php echo get_the_date() ?>
        </time>
        <meta itemprop="datePublished" content="<?php echo esc_attr(get_the_date('Y-m-d H:i:s')) ?>">
    </a>
</span>
<?php endif ?>