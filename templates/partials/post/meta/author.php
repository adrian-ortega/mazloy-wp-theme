<span class="entry__meta-item entry__meta-item--author" itemscope="http://schema.org/Person" itemprop="author">
    <span class="entry-by"><?php _e('By', mazloy('textdomain')) ?></span>
    <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')) ?>" class="entry-author-name" title="See more posts by <?php echo get_the_author_meta('display_name') ?>">
        <span itemprop="name"><?php echo get_the_author_meta('display_name') ?></span>
    </a>
    <link itemprop="sameAs" href="<?php echo get_author_posts_url(get_the_author_meta('ID')) ?>">
</span>