<?php if($categories_list = get_the_category_list( null )):?>
<div class="entry__meta-item entry__meta-item--categories">
    <span class="icon-flag">
        <i class="mdi mdi-folder-multiple-outline"></i>
    </span>
    <span class="screen-reader-text" aria-hidden="true">Post Categories</span>
    <nav>
	    <?php echo $categories_list ?>
    </nav>

</div>
<?php endif;
