<?php
/**
 * This template that displays the "no posts found" message.
 * Can be easily called by the gc_partial('post.none') function.
 */