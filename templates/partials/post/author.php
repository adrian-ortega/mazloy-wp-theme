<?php
/**
 * @var WP_User $authordata
 */

if(mazloy_get_option('blog_show_author', '1') != '1')
    return null;

global $authordata;
$avatar_size = mazloy_get_option('blog_author_avatar_size', 100);
$link_title = sprintf( __( 'More posts by %s', mazloy( 'textdomain' ) ), $authordata->display_name );
$link_url = esc_url( get_author_posts_url( $authordata->ID, $authordata->user_nicename ) );
$description = apply_filters( 'the_author_description', get_the_author_meta( 'description', $authordata->ID ) );

?>
<div class="entry__author author">
    <?php if(mazloy_get_option('blog_author_title', '0') == '1') : ?>
        <?php $section_title = mazloy_get_option('blog_author_section_title', __('More about the Author', mazloy('textdomain'))); ?>
        <h2 class="sub-section-title"><?php echo $section_title  ?></h2>
    <?php endif ?>
	<div class="row">
        <div class="col-xs col-md-2">
            <a href="<?php echo $link_url ?>" class="author__avatar" title="<?php echo $link_title ?>">
                <?php echo get_avatar($authordata->ID, $avatar_size) ?>
            </a>
        </div>
        <div class="col-xs col-md-9">
            <h3 class="author__title"><?php printf(__('Written By %s', mazloy('textdomain')), "<span>{$authordata->display_name}</span>") ?></h3>
            <?php if(!empty($description)) :?>
            <div class="author__description">
	            <?php echo apply_filters('the_content', $description) ?>
            </div>
            <?php endif ?>
        </div>
    </div>
</div>