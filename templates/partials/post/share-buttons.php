<?php
$button_options = json_encode([
    'title' => get_the_title(),
    'url' => get_the_permalink()
]);
$networks = [
    'facebook' => 'Facebook',
    'twitter' => 'Twitter',
    'google-plus' => 'Google Plus',
    'linkedin' => 'LinkedIn'
] ?>
<nav class="entry__meta-item entry__meta-item--share-buttons" data-share-buttons='<?php echo $button_options ?>'>
    <span class="entry__meta-label entry__meta-label--share-buttons">
        <span class="screen-reader-text"><?php _e( 'Share', mazloy('textdomain') ) ?></span>
        <span class="icon"><i class="mdi mdi-share"></i></span>
    </span>
    <?php foreach($networks as $network_id => $network):?>
        <?php $icon = str_replace('_', '-', $network_id); ?>
        <button
            class="button button--share button--share-<?php echo $icon ?>"
            data-share-button="<?php echo $network_id ?>"
            title="<?php printf(__('Share on %s', mazloy('textdomain')), $network) ?>">
            <span class="icon"><i class="mdi mdi-<?php echo $icon ?>"></i></span>
            <span class="text"><?php echo $network ?></span>
        </button>
    <?php endforeach ?>
</nav>