<article id="post-<?php the_ID() ?>" <?php post_class(['entry', 'entry--preview', 'entry--related']) ?> data-equalizer-watch>

    <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
        <?php mazloy_post_thumbnail('mazloy-preview', true, false) ?>
    </a>

    <header class="entry__header">
        <h1 class="entry__title">
            <a href="<?php the_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a>
        </h1>
        <div class="entry__meta">
            <?php mazloy_partial('post.meta.author') ?>
            <?php mazloy_partial('post.meta.date') ?>
        </div>
    </header>

    <section class="entry__content">
        <p class="read-more">
            <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                <?php _e('Continue reading', mazloy('textdomain')) ?>
                <i class="fa fa-long-arrow-right"></i>
            </a>
        </p>
    </section>
</article>