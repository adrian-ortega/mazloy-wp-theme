<?php if(!mazloy_enabled('social_header_enable')): return; endif ?>
<?php $mod = isset($mod) ? $mod : '';
$classes = ['social-links'];
if(!empty($mod)) {
    $classes[] = 'social-links--' . $mod;
}
?>
<nav<?php mazloy_classes($classes)?> itemscope itemtype="http://schema.org/Organization">
    <link itemprop="url" href="<?php echo get_home_url() ?>">
	<?php foreach(mazloy_get_social_networks() as $network) :?>
        <?php $attributes = [
            'href' => $network->url,
            'itemprop' => 'sameAs',
            'title' => $network->alt_text,
            'class' => [
                'social-link',
                "social-link--{$network->id}"
            ]
        ];

        if(!empty($mod)) {
            $attributes['class'][] = 'social-link--' . $mod;
        }?>

		<a <?php echo mazloy_get_html_attrs($attributes) ?>>
			<span class="social-link__icon"><?php echo $network->icon ?></span>
			<span class="social-link__text"><?php echo $network->name ?></span>
		</a>
	<?php endforeach ?>
</nav>
