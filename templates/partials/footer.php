<footer class="footer footer-main">
    <div class="footer__widgets">
        <div class="container">
            <div class="row">
	            <?php for($i = 1; $i <= 4; $i++) : ?>
                    <?php if(is_active_sidebar('footer-'. $i)):?>
                    <div class="col-xs">
                        <div <?php mazloy_classes([
                            'widget-area',
                            'footer-widget-area',
                            'footer-widget-area-' . $i
                        ]) ?>>
                            <?php dynamic_sidebar('footer-' . $i) ?>
                        </div>
                    </div>
                    <?php endif ?>
	            <?php endfor ?>
            </div>
        </div>
    </div>
    <div class="footer__copyright">
        <div class="container">
            <div class="row">
                <div class="col-xs col-md-6">
                    <?php mazloy_copyright_menu() ?>
                    <p class="copyright">&copy; <?php echo date('Y') ?> <?php bloginfo('name') ?></p>
                </div>
                <div class="col-xs col-md-6">
	                <?php mazloy_partial('social-links', ['mod' => 'footer']) ?>
                </div>
            </div>
        </div>
    </div>
</footer>