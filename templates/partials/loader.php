<?php $type = mazloy_get_option('loader_type') ;?>
<div class="mazloy-loader mazloy-loader--<?php echo $type ?>">
    <?php if(in_array($type, ['material-bars', 'material-bars-colors'])) : ?>
        <?php echo str_repeat('<div class="mazloy-loader__part"></div>', 4) ?>
    <?php else: ?>
        <div class="mazloy-loader__part"></div>
    <?php endif ?>
    <?php if(mazloy_enabled('loader_show_text')): ?>
	<div class="mazloy-loader__text"><?php mazloy_option('loader_text' ) ?></div>
    <?php endif ?>
</div>

<div class="mazloy-loader-overlay"></div>
<script>
    setTimeout(function() { document.body.classList.remove('mazloy-loader-show'); }, 1000);
</script>