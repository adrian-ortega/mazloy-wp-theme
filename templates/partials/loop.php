<?php if(have_posts()): ?>
<div class="entries">
    <?php while(have_posts()): the_post() ?>
        <?php mazloy_partial('post.preview') ?>
    <?php endwhile ?>
</div>
<?php else: mazloy_partial('post.none'); endif ?>