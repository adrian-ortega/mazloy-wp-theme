<?php while(have_posts()): the_post(); ?>
    <?php
    $metabox = mazloy( 'metabox_page_rows' );
    $options = $metabox->get_meta();
    $full_screen = $options['full_screen'] == '1';
    ?>

	<article <?php post_class() ?> id="page-<?php the_ID() ?>">
		<?php mazloy_partial('layout.page-header') ?>
        <div class="<?php echo $full_screen ?  'container-fluid' : 'container' ?>">
            <?php the_content() ?>
        </div>
	</article>
<?php endwhile;