<h4>Here are a few of our recent blog posts</h4>
<?php
wp_reset_query();
$_query = new WP_Query([
    'post_type' => 'post',
    'posts_per_page' => 5,
    'orderby' => 'date',
    'order' => 'DESC'
]);
if ($_query->have_posts()): ?>
    <nav>
        <ul>
            <?php while ($_query->have_posts()): $_query->the_post() ?>
                <li><a href="<?php echo get_permalink() ?>" title="<?php the_title() ?>"><?php the_title() ?></a></li>
            <?php endwhile ?>
        </ul>
    </nav>
<?php endif;
wp_reset_query(); ?>