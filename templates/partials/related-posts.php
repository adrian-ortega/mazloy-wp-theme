<?php

if(mazloy_get_option('blog_show_related', '0') != '1')
    return null;

global $post;
$categories = [];

foreach(get_the_category() as $term)
    $categories[] = $term->term_id;

$related = new WP_Query(array(
    'posts_per_page' => mazloy_get_option('blog_related_count', 3),
    'post__not_in' => array($post->ID),
    'category__in' => $categories
));

if(!$related->have_posts())
    return;
?>
<div class="entries entries--related">
    <?php if(($block_title = mazloy_get_option('blog_related_section_title', '')) && !empty($block_title)):?>
    <h2><?php echo $block_title ?></h2>
    <?php endif ?>
    <div class="row between-xs">
    <?php while($related->have_posts()): $related->the_post(); ?>
        <div class="col-xs">
            <?php mazloy_partial('post.preview-related', get_post_format()) ?>
        </div>
    <?php endwhile; wp_reset_query() ?>
    </div>
</div>