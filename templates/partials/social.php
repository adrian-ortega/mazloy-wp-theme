<?php if( ($social = []) && count($social) > 0):?>
    <div class="social-links">
        <?php foreach($social as $network): ?>
            <a href="<?php echo $network->url ?>" target="_blank" class="social-link social-link-<?php echo $network->type ?>" title="<?php echo $network->name ?>">
                <?php $network->icon() ?>
                <span class="screen-reader-text"><?php echo $network->name ?></span>
            </a>
        <?php endforeach ?>
    </div>
<?php endif; ?>