<h2 class="comments-title">
<?php
$post_title = sprintf('<span class="comments-title-post-name">%s</span>', get_the_title())
?>
<?php if (($comments_number = get_comments_number()) && '1' === $comments_number ):
    printf( _x( 'One Reply to %s', 'comments title', 'twentyseventeen' ), $post_title ); ?>
<?php else :
    printf(
        _nx(
            '<span class="comments-title-count">%1$s</span> Reply to %2$s',
            '<span class="comments-title-count">%1$s</span> Replies to %2$s',
            $comments_number,
            'comments title',
            mazloy('textdomain')
        ),
        number_format_i18n( $comments_number ),
        $post_title
    );
endif ?>
</h2>