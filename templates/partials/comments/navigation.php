<?php
the_comments_pagination( [
    'prev_text' => '<span class="previous comments-pagination-button">' . __( 'Previous', mazloy('textdomain') ) . '</span>',
    'next_text' => '<span class="next comments-pagination-button">' . __( 'Next', mazloy('textdomain') ) . '</span>',
]);