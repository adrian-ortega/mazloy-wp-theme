<?php
/**
 * @var \Mazloy\Metaboxes\PageHeaderMetabox $metabox
 */

use Mazloy\Core\InlineCss\Css;
use Mazloy\Core\Markup;

$_id     = isset( $_id ) ? $_id : get_the_ID();

if(mazloy( 'metabox_page_rows' )->get_meta('full_screen', $_id) == '1')
	return null;

$metabox   = mazloy( 'metabox_page_header' );
$options   = $metabox->get_meta(null, $_id);
$sub_title = isset($sub_title) ? $sub_title : '';

$attributes     = [
	'class' => [
		'page-header'
	]
];
$row_attributes = [
    'class' => ['row']
];

if(!empty($options['scroll_effect'])) {
	$attributes['class'][] = 'page-header--' . $options['scroll_effect'];
}

if(isset($options['header_align']['horizontal'])) {
    $attributes['class'][] = 'page-header--horz-' . $options['header_align']['horizontal'];
	switch($options['header_align']['horizontal']) {
		case 'left': $row_attributes['class'][] = 'start-xs'; break;
		case 'center': $row_attributes['class'][] = 'center-xs'; break;
		case 'right': $row_attributes['class'][] = 'end-xs'; break;
	}
}

if(isset($options['header_align']['vertical'])) {
	$attributes['class'][] = 'page-header--vert-' . $options['header_align']['vertical'];
	switch($options['header_align']['vertical']) {
        case 'top': $row_attributes['class'][] = 'top-xs'; break;
        case 'center': $row_attributes['class'][] = 'middle-xs'; break;
        case 'bottom': $row_attributes['class'][] = 'bottom-xs'; break;
    }
}

if(!empty($options['header_height'])) {
	$attributes['style'] = [
		'height' => $options['header_height'] . 'px'
	];
}

$title_attributes = [
	'class' => 'page-header__title'
];

if(!empty($options['header_font_color'])) {
	$title_attributes['style'] = [
		'color' => $options['header_font_color']
	];
}

if(!empty($sub_title)) {
    $attributes['class'][] = 'page-header--has-sub-title';
}
?>
<header<?php mazloy_html_attrs($attributes) ?>>
	<div class="container">
        <div<?php mazloy_html_attrs($row_attributes) ?>>
            <div class="col-xs">
                <?php echo Markup::tag('h1', $title_attributes, get_the_title($_id)) ?>
                <?php echo !empty($sub_title) ? Markup::tag('h2', array_merge($title_attributes, [ 'class' => 'page-header__sub-title']), $sub_title) : ''?>
            </div>
        </div>
	</div>
	<?php

	if(!empty($options['header_overlay_color'])) {
		$opacity = isset($options['header_overlay_opacity']) ? $options['header_overlay_opacity'] : 10;
		$opacity = intval($opacity) / 100;
		echo Markup::tag( 'div', [
			'class' => 'page-header__overlay',
			'style' => [
				'background-color' => Css::rgba( $options['header_overlay_color'], $opacity )
			]
		], '' );
	}

	if($options['background_type'] === 'image') {
		$page_header_image_classes = ['page-header__image'];
		echo Markup::tag( 'div', [
			'class' => $page_header_image_classes,
			'style' => [
				'background-image' => "url('{$options['background_image']['url']}')"
			]
		], '' );
	} ?>
</header>