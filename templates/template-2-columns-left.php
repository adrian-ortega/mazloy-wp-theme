<?php
/**
 * Template Name: Two Columns - Left
 */
?>
<div class="container">
	<div class="row">
		<div class="col-xs col-md-3">
			<?php get_sidebar('sidebar-left') ?>
		</div>
		<div class="col-xs col-md-9">
			<?php mazloy_partial('content', get_post_type()) ?>
		</div>
	</div>
</div>