<form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')) ?>">
    <div class="search-input">
        <label>
            <span class="screen-reader-text"><?php echo _x('Search for:', 'label', mazloy('textdomain')) ?></span>
            <input type="search" class="search-field"
                   value="<?php echo get_search_query() ?>"
                   name="s"
                   placeholder="<?php echo esc_attr_x('Search blog', 'placeholder', mazloy('textdomain')) ?>"
                   title="<?php echo esc_attr_x('Search blog', 'label', mazloy('textdomain')) ?>"/>
        </label>
    </div>
    <div class="search-button">
        <button type="submit" class="button search-submit">
            <span class="screen-reader-text">
                <?php echo esc_attr_x('Search', 'submit button', mazloy('textdomain')) ?>
            </span>
            <i class="fa fa-search"></i>
        </button>
    </div>
</form>