<?php if ( post_password_required() ) return; ?>

<div class="comments">
    <?php if ( have_comments() ) : ?>
        <?php mazloy_partial('comments.title') ?>
        <ol class="comments-list">
            <?php wp_list_comments([
                'avatar_size' => 100,
                'style'       => 'ol',
                'short_ping'  => true,
                'reply_text'  => __( 'Reply', mazloy('textdomain')),
            ]); ?>
        </ol>
        <?php mazloy_partial('comments.navigation') ?>

    <?php endif; if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) : ?>
        <p class="comments-none"><?php _e( 'Comments are closed.', mazloy('textdomain') ); ?></p>
    <?php endif; ?>
    <?php comment_form(); ?>
</div>
