<article <?php post_class(['entry', 'entry--single']) ?> id="post-<?php the_ID(); ?>">
    <?php while (have_posts()): the_post(); ?>
        <?php mazloy_partial('post.header', [ 'thumbnail_size' => 'mazloy-single' ]) ?>

        <div class="entry__content">
            <?php the_content() ?>
        </div>

        <?php if(mazloy_is_categorized()):?>
        <footer class="entry__footer">
            <div class="entry__meta">
                <?php mazloy_partial('post.meta.categories') ?>
                <?php mazloy_partial('post.share-buttons') ?>
            </div>
        </footer>
        <?php endif ?>
    <?php endwhile ?>
</article>
<aside>
    <?php mazloy_partial('post.author') ?>
</aside>
<aside>
    <?php mazloy_partial('related-posts') ?>
</aside>

<?php comments_template(); ?>

