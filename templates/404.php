<header>
    <h1 class="entry-title"><?php echo sprintf(__('Page %s Not Found', mazloy('textdomain')), get_query_var('name')) ?></h1>
</header>
<article <?php post_class('entry') ?> id="page-<?php the_ID() ?>">
    <h4>Whoops</h4>
    <p>
        Looks like the page you were looking for doesn't exist. Maybe try searching
        for something else using the search bar.
    </p>
    <?php get_search_form() ?>
    <?php mazloy_partial('404.recent-posts') ?>
</article>