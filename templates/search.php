<header>
    <h1 class="page-title">
    <?php if ( have_posts() ) : ?>
        <?php printf( __( 'Search Results for: %s', mazloy('textdomain') ), '<span>' . get_search_query() . '</span>' ); ?>
    <?php else : ?>
        <?php _e( 'Nothing Found', mazloy('textdomain') ); ?>
    <?php endif; ?>
    </h1>
</header>
<?php mazloy_partial('loop')?>