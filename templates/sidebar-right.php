<?php if (is_active_sidebar('sidebar-right')): ?>
	<?php
	$sidebar_classes = ['widget-area', 'widget-sidebar-area', 'widget-sidebar-area--right'];

	if(is_single())
		$sidebar_classes[] = 'widget-sidebar-area--single';

	if(is_archive())
		$sidebar_classes[] = 'widget-sidebar-area--archive';

	if(is_search())
		$sidebar_classes[] = 'widget-sidebar-area--search';

	$sidebar_classes = join(' ', $sidebar_classes);
	?>

	<div class="<?php echo $sidebar_classes ?>" role="complementary">
		<?php dynamic_sidebar('sidebar-right'); ?>
	</div>
<?php endif;