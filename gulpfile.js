const gulp = require('gulp');
const config = require('./package.json');
const args = require('yargs').argv;
let plugins = require('gulp-load-plugins')();

config.env = {
    production: !!args.production,
    development: !args.production
};

plugins.browserSync = require('browser-sync');

const getTask = file => require(`./build/gulp/${file}.js`)(gulp, plugins, config);

gulp.task('vendors', getTask('util/copy-vendors'));
gulp.task('front-styles', getTask('front/styles'));
gulp.task('admin-styles', getTask('admin/styles'));
gulp.task('uglify', getTask('uglify'));

gulp.task('build-front', ['front-styles']);
gulp.task('build-admin', ['admin-styles']);

gulp.task('build', ['build-admin', 'build-front']);
gulp.task('watch', getTask('watch'));

gulp.task('theme', ['build', 'uglify'], getTask('theme'));

gulp.task('default', ['build', 'watch']);