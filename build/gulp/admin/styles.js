module.exports = (gulp, plugins, config) => {
    const sassConfig = {
        outputStyle: config.env.production ? 'compressed' : 'expanded',
        errLogToConsole: true,
        includePaths: [

        ]
    };

    // All customizer classes are name-spaced in JS, we need to match
    // the same thing with css so that classes can be targeted
    // properly. `gulp-header` helps us by printing the scss variables
    // before compiling the source scss files.
    let namespace = config.theme.short_name.toLowerCase();
    if(namespace.length === 0 ) namespace = 'aod';

    let scssHeader = [
        `$customizer-prefix: "${namespace}";`,
        "$customizer-block: #{$customizer-prefix + '-customize'};",
    ].join('\n');

    return function() {
        return gulp.src(config.paths.styles.src + '/admin/admin.scss')
            .pipe(plugins.if(config.env.development, plugins.sourcemaps.init()))
            // .pipe(plugins.header(scssHeader))
            .pipe(plugins.sass(sassConfig).on('error', plugins.sass.logError))
            .pipe(plugins.if(config.env.production, plugins.rename({ suffix: '.min' })))
            .pipe(plugins.if(config.env.development, plugins.sourcemaps.write()))
            .pipe(gulp.dest(config.paths.styles.dist))
            .pipe(plugins.browserSync.reload({stream: true}))
            .pipe(plugins.notify({
                message: "Successfully compiled " + (config.env.production ? 'and minified ':'') +"admin styles"
            }));
    }
};