module.exports = (gulp, plugins, config) => {
    return function() {
        return gulp.src([
            "assets/**",
            "!assets/_src/**/*",
            "!assets/_src",
            "inc/**",
            "templates/**",
            "package.json",
            "functions.php",
            "index.php",
            "screenshot.png",
            "style.css"
        ], { base: "." })
            .pipe(plugins.zip('mazloy.zip'))
            .pipe(gulp.dest('../.'))
            .pipe(plugins.notify({
                message: "Successfully created the WordPress Theme ZIP"
            }));
    }
};