module.exports = (gulp, plugins, config) => {
    const vendors = [
        //[`${config.paths.npm}/normalize-scss/sass/*`, `${config.paths.styles.src}/vendors`],
        [`${config.paths.npm}/slick-carousel/slick/*.js`, `${config.paths.scripts.dist}/vendors/slick`],
        [`${config.paths.npm}/slick-carousel/slick/fonts/*`, `${config.paths.assets}/fonts`],
        [`${config.paths.npm}/slick-carousel/slick/*.scss`, `${config.paths.styles.src}/vendors/slick`]
    ];

    vendors.forEach(function(dep) {
        gulp.src(dep[0]).pipe(gulp.dest(dep[1]));
    });
};