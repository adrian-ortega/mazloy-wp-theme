module.exports = (gulp, plugins, config) => {
    const sassConfig = {
        outputStyle: config.env.production ? 'compressed' : 'expanded',
        errLogToConsole: true,
        includePaths: [

        ]
    };
    return () =>{
        gulp.src(config.paths.styles.src + '/frontend/app.scss')
            .pipe(plugins.if(config.env.development, plugins.sourcemaps.init()))
            .pipe(plugins.sass(sassConfig).on('error', plugins.sass.logError))
            .pipe(plugins.if(config.env.production, plugins.rename({ suffix: '.min' })))
            .pipe(plugins.if(config.env.development, plugins.sourcemaps.write()))
            .pipe(gulp.dest(config.paths.styles.dist))
            .pipe(plugins.browserSync.stream({match: '**/*.css'}))
            .pipe(plugins.notify({
                message: "Successfully compiled " + (config.env.production ? 'and minified ' : '') + "styles"
            }));
    }
};