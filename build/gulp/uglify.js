module.exports = (gulp, plugins, config) => {
    return () => {
        ["admin.js", "app.js"].forEach( file => {
            gulp.src([
                `${config.paths.scripts.dist}/${file}`,
            ])
                .pipe(plugins.babel({
                    presets: ["env"]
                }))
                .pipe(plugins.uglify())
                .pipe(plugins.rename({ suffix: '.min' }))
                .pipe(gulp.dest(config.paths.scripts.dist))
                .pipe(plugins.notify({
                    message: `Successfully minified ${file}`
                }));
        });
    }
};