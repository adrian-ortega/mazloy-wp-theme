module.exports = (gulp, plugins, config) => {
    return () => {
        plugins.browserSync.init([
            config.paths.base + '/**/*.php'
        ], {
            proxy: "wp2.dev:8888"
        });

        let commonFiles = [
            config.paths.styles.src + '/_settings.scss',
            config.paths.styles.src + '/_mixins.scss',
            config.paths.styles.src + '/_theme.scss',
            config.paths.styles.src + '/vendors/**/*.scss',
            config.paths.styles.src + '/util/**/*.scss',
        ];

        // Watch .scss files
        gulp.watch([
            config.paths.styles.src + '/admin/**/*.scss'
        ].concat(commonFiles), ['admin-styles']);

        gulp.watch([
            config.paths.styles.src + '/frontend/**/*.scss'
        ].concat(commonFiles), ['front-styles']);

        // Watch .js files
        // gulp.watch([
        //     config.paths.scripts.src + '/admin/**/*.js'
        // ], ['admin-scripts']);

        // gulp.watch([
        //     config.paths.scripts.src + '/frontend/**/*.js'
        // ], ['scripts']);

        // Watch images
        // gulp.watch([
        //     config.paths.images.src + '/**/*.{png,jpg,gif}'
        // ], ['images']);

        // Watch SVG files
        // gulp.watch([
        //     config.paths.svg.src + '/**/*.svg'
        // ], ['svg-sprite']);
    }
};