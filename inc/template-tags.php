<?php

/**
 * Includes a partial saved in `/templates/partials`
 * @param null|string $path
 * @param null|string|array $variant
 * @param array $args
 * @return void
 */
function mazloy_partial($path = null, $variant = null, $args = []) {
	if(empty($path))
		return;

	// Trade variant and args
	if(is_array($variant)) {
		$args = $variant;
		$variant = null;
	}

	$path = \Mazloy\Core\DotNotation::dotToPath(str_replace([
		'templates/partials',
		'templates.partials',
		'partials/',
		'partials.',
		'.php'
	], '', $path));

	$prefix = mazloy('theme.path') . 'templates/partials';
	$templates = [];
	$variant = (string) $variant;

	// Adds variants tot he templates array first so they're searched first
	if(!empty($variant))
		$templates[] = "{$prefix}/{$path}-{$variant}.php";

	$templates[] = "{$prefix}/{$path}.php";
	$located = false;

	foreach($templates as $template) {
		if(!$template)
			continue;

		if(file_exists($template)) {
			$located = $template;
			break;
		}
	}

	if($located !== false) {
		// Keep the same functionality as load_templae
		global $posts, $post, $wp_did_header, $wp_query,
		       $wp_rewrite, $wpdb, $wp_version, $wp, $id,
		       $comment, $user_ID;

		if(is_array($wp_query->query_vars))
			extract($wp_query->query_vars, EXTR_SKIP);

		// Includes extra parameters that we passed
		if(!empty($args)) {
			extract((array) $args, EXTR_SKIP);
		}

		include($located);
	}
}

/**
 * Returns all data of an image and size;
 * @param null|int|WP_Post $post
 * @param string $size
 * @return bool|\Mazloy\Assets\PostImage
 */
function mazloy_get_post_image_attachment($post = null, $size = 'thumbnail') {
	if (empty($post))
		return false;

	if (!is_object($post))
		$post = get_post($post);

	$image = new \Mazloy\Assets\PostImage($post);
	return $image->setSize($size)->build();
}

/**
 * Wraps the post thumbnail in an anchor element for previews or a div
 * @param string $size
 * @param bool $background
 * @param bool $aspect
 * @return string
 */
function mazloy_get_post_thumbnail($size = 'full', $background = false, $aspect = false) {
	if (post_password_required() || is_attachment() || !has_post_thumbnail())
		return false;

	$post = get_post();
	$image = mazloy_get_post_image_attachment($post, $size);

	if(!$image->ID)
		return false;

	$classes = ['entry__thumbnail'];
	if($background) {
		$classes[] = 'entry__thumbnail--has-background';
	}

	$html = '<';
	$html .= is_singular() ? 'div' : 'a href="' . get_the_permalink() . '" title="' . esc_attr($post->post_title) . '"';
	$html .= ' class="' . implode(' ', $classes) . '"';

	if($background) {
		$html .= ' style="background-image:url(\'' . $image->url . '\');';

		if($aspect)
			$html .= 'padding-bottom: ' . $image->aspect_ratio() . '%';

		$html .= '"';
	}

	$html .= '>';

	if(!$background) {
		$html .= '<img src="'. $image->url . '" alt="' . $post->post_title . '"/>';
	}

	$html .= '</' . (is_singular() ? 'div' : 'a') . '>';

	return $html;
}

/**
 * Returns image HTML for a featured image
 * @param string $size
 * @param bool $background
 * @param bool $aspect
 */
function mazloy_post_thumbnail($size = 'large', $background = false, $aspect = false) {
	echo mazloy_get_post_thumbnail($size, $background, $aspect);
}

/**
 * Returns a theme image url
 * @param $image
 * @return string
 */
function mazloy_get_image($image) {
	return mazloy('assets.images') . $image;
}

/**
 * Prints a theme image url
 * @param string$image
 * @return void
 */
function mazloy_image($image) {
	echo mazloy_get_image($image);
}

/**
 * Returns the HTML for an svg icon
 * @param null|string $icon
 * @param array $args
 * @param bool $echo
 * @return string
 */
function mazloy_svg($icon = null, $args = [], $echo = true) {
	if(empty($icon))
		return '';

	$args = wp_parse_args( $args, [
		'title'       => '',
		'desc'        => '',
		'fallback'    => false,
		'attr'        => '',
		'xlink'       => true,
	] );

	$aria_hidden = ' aria-hidden="true"';

	// Set ARIA.
	$aria_labelledby = '';
	$unique_id = false;

	if ( $args['title'] ) {
		$aria_hidden = '';
		$unique_id = uniqid();
		$aria_labelledby = ' aria-labelledby="title-' . $unique_id . '"';

		if ( $args['desc'] ) {
			$aria_labelledby = ' aria-labelledby="title-' . $unique_id . ' desc-' . $unique_id . '"';
		}
	}

	$icon = str_replace('.svg', '', $icon);
	$svg_class = ['icon', 'icon-' . esc_attr($icon) ];

	if($args['xlink'] !== true)
		$svg_class[] = 'icon-raw-svg';

	$svg_class = join(' ', $svg_class);
	$svg = '<svg class="'. $svg_class . '"' . $aria_hidden . $aria_labelledby;

	if(!empty($args['attr']))
		$svg .= $args['attr'];

	$svg .= ' role="img">';

	if ( $args['title'] ) {
		$svg .= '<title id="title-' . $unique_id . '">' . esc_html( $args['title'] ) . '</title>';

		// Display the desc only if the title is already set.
		if ( $args['desc'] ) {
			$svg .= '<desc id="desc-' . $unique_id . '">' . esc_html( $args['desc'] ) . '</desc>';
		}
	}

	switch ($args['xlink']) {
		case 'raw':
		case false:
			$svg_src = $args['xlink'] === 'raw' ? '_src' : 'images';
			$icon = str_replace('.svg', '', $icon);
			$icon_path = mazloy('theme.path') . "assets/{$svg_src}/svg/{$icon}.svg";
			$icon_raw = file_get_contents($icon_path);

			if(preg_match('#<\s*?svg\b[^>]*>(.*?)</svg\b[^>]*>#s', $icon_raw, $matches)) {
				$svg .= str_replace(["\r", "\n"], '', trim($matches[1]));
			}
			break;
		default:
			$svg .= ' <use href="#' . esc_html( $icon ) . '" xlink:href="#' . esc_html( $icon ) . '"></use> ';
			break;
	}

	// Add some markup to use as a fallback for browsers that do not support SVGs.
	if ( $args['fallback'] ) {
		$svg .= '<span class="svg-fallback icon-' . esc_attr( $icon ) . '"></span>';
	}

	$svg .= '</svg>';

	if($echo)
		echo $svg;

	return $svg;
}

/**
 * Returns the class attribute for the content wrapper. This is essentially a smaller
 * version of body_class()
 * @param array $more
 * @param string $base
 */
function mazloy_content_classes($more = [], $base = 'content') {
	$classes = [$base];

	if('' != ($template_slug = get_page_template_slug())) {
		$template_slug = str_replace([ 'templates/', '.php' ], '', $template_slug);
		$classes[] = $base . '-' . $template_slug;
	}

	if(is_single() || is_page()) {
		$post = get_post();
		$classes[] = "{$base}-{$post->post_name}";
		$classes[] = "{$base}-{$post->ID}";
		$classes[] = "{$base}-{$post->post_type}-{$post->ID}";
	}

	$classes[] = $base . '-' . get_post_type();

	if ( is_front_page() )
		$classes[] = "{$base}-home";
	if ( is_home() )
		$classes[] = "{$base}-blog";
	if ( is_archive() )
		$classes[] = "{$base}-archive";
	if ( is_date() )
		$classes[] = "{$base}-date";

	echo \Mazloy\Core\Markup::classes(array_unique(array_map('esc_attr', array_merge($classes, (array) $more))));
}

/**
 * Checks to see if a blog post is categorized
 * @return bool
 */
function mazloy_is_categorized() {
	static $category_count = false;

	if ( false === $category_count ) {
		$category_count = count(get_categories([
			'fields'     => 'ids',
			'hide_empty' => 1,
			'number'     => 2,
		]));
	}

	return $category_count > 1;
}

function mazloy_classes($classes = array()) {
	echo \Mazloy\Core\Markup::classes($classes);
}

function mazloy_options($key = null, $post_id = null) {
	if(empty($post_id) || is_numeric($post_id))
		$post_id = get_post($post_id)->ID;

	return get_post_meta($post_id, $key, true);
}

/**
 * Prints out the main menu
 */
function mazloy_main_menu() {
	if(has_nav_menu('main')) {
		echo '<nav id="main-nav" itemscope itemtype="http://schema.org/SiteNavigationElement">';
		wp_nav_menu( [
			'container'      => '',
			'menu_class'     => 'menu menu--main menu--horizontal',
			'theme_location' => 'main',
			'depth'          => 5,
			'walker'         => new \Mazloy\Walkers\TopBarMenu(),
			'items_wrap'     => '<ul role="menubar" id="%1$s" class="%2$s">%3$s</ul>',
			'link_before'    => '<span itemprop="name">',
			'link_after'    => '</span>',
		] );
		echo '</nav>';
	} else {
		echo '<p>No main menu assigned</p>';
	}
}

/**
 * Prints out the utility menu
 */
function mazloy_utility_menu() {
	if(has_nav_menu('utility')) {
		wp_nav_menu( [
			'container'      => '',
			'menu_class'     => 'menu menu--utility menu--horizontal',
			'theme_location' => 'utility',
			'depth'          => 5,
			'walker'         => new \Mazloy\Walkers\TopBarMenu(),
			'items_wrap'     => '<ul role="menubar" id="%1$s" class="%2$s">%3$s</ul>',
			'link_before'    => '<span itemprop="name">',
			'link_after'    => '</span>',
		] );
	}
}

/**
 * Prints out the utility menu
 */
function mazloy_copyright_menu() {
	if(has_nav_menu('copyright')) {
		wp_nav_menu( [
			'container'      => '',
			'menu_class'     => 'menu menu--copyright menu--horizontal',
			'theme_location' => 'copyright',
			'depth'          => 5,
			'walker'         => new \Mazloy\Walkers\CopyrightMenu(),
			'items_wrap'     => '<ul role="menubar" id="%1$s" class="%2$s">%3$s</ul>',
			'link_before'    => '<span itemprop="name">',
			'link_after'    => '</span>',
		] );
	}
}