<?php
/**
 * This theme uses Composer as the PHP dependency manager.
 */
require_once "vendor/autoload.php";

/**
 * This theme requires at least 5.5.12
 */
if(!version_compare('5.5.12', phpversion(), '<=')) {
	Mazloy\Theme::stop(
		sprintf(__( 'You must be using PHP 5.5.12 or greater, currently running %s' ), phpversion()),
		__('Invalid PHP Version', 'aod')
	);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Debug
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if(!function_exists('pre')) {
	function pre($arg) {
		echo '<pre>';
		(is_bool($arg) || is_null($arg)) ? var_dump($arg) : print_r($arg);
		echo '</pre>';
	}
}

if(!function_exists('dd')) {
	function dd() {
		foreach( func_get_args() as $arg )
			pre($arg);
		die();
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Template Helpers
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Returns an instance of the theme
 * @param null|string $name
 * @param null|string|callable $value
 * @return \Mazloy\Theme|mixed
 */
function mazloy($name = null, $value = null ) {
	$instance = \Mazloy\Theme::getInstance();
	$container = $instance->container();

	if( !empty($value) )
		return $container->set($name, $value);

	if( !empty($name) ) {
		return $container->get($name);
	}

	return $instance;
}

/**
 * Starts the theme
 * @return void
 */
function wm_start() {
	Mazloy\Theme::start();

	/**
	 * When the theme is first activated, we want to make sure that the template path is updated
	 * to aod/templates rather than aod so that WordPress will look for these files in the correct directory
	 */
	if (basename($stylesheet = get_option('template')) !== 'templates') {
		update_option('template', "{$stylesheet}/templates");
		wp_redirect($_SERVER['REQUEST_URI']);
		exit();
	}
}