<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit8749344a8782a77924a3c8640c3894fc
{
    public static $files = array (
        'a5f882d89ab791a139cd2d37e50cdd80' => __DIR__ . '/..' . '/tgmpa/tgm-plugin-activation/class-tgm-plugin-activation.php',
        'abd4e993b43ea7b3132be7cee135270a' => __DIR__ . '/../../..' . '/inc/helpers.php',
        '9b9856c7820371ac0ec866871d967feb' => __DIR__ . '/../../..' . '/inc/customizer.php',
        '858801248b81a517c8384ff02a6b3df3' => __DIR__ . '/../../..' . '/inc/template-tags.php',
        '29d42d84d4cbe71f6246c2ed91919371' => __DIR__ . '/..' . '/redux-framework/framework.php',
    );

    public static $prefixLengthsPsr4 = array (
        'M' => 
        array (
            'Mazloy\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Mazloy\\' => 
        array (
            0 => __DIR__ . '/../../..' . '/inc/lib',
        ),
    );

    public static $classMap = array (
        'Mazloy\\Admin\\Options' => __DIR__ . '/../../..' . '/inc/lib/Admin/Options.php',
        'Mazloy\\Admin\\Options\\Helpers\\General' => __DIR__ . '/../../..' . '/inc/lib/Admin/Options/Helpers/General.php',
        'Mazloy\\Admin\\Options\\Helpers\\GoogleFonts' => __DIR__ . '/../../..' . '/inc/lib/Admin/Options/Helpers/GoogleFonts.php',
        'Mazloy\\Admin\\Plugins' => __DIR__ . '/../../..' . '/inc/lib/Admin/Plugins.php',
        'Mazloy\\Admin\\PostsTables' => __DIR__ . '/../../..' . '/inc/lib/Admin/PostsTables.php',
        'Mazloy\\Assets\\PostImage' => __DIR__ . '/../../..' . '/inc/lib/Assets/PostImage.php',
        'Mazloy\\Assets\\Slide' => __DIR__ . '/../../..' . '/inc/lib/Assets/Slide.php',
        'Mazloy\\Assets\\SocialIcon' => __DIR__ . '/../../..' . '/inc/lib/Assets/SocialIcon.php',
        'Mazloy\\Core\\Abstracts\\MetaboxAbstract' => __DIR__ . '/../../..' . '/inc/lib/Core/Abstracts/MetaboxAbstract.php',
        'Mazloy\\Core\\Abstracts\\RunableAbstract' => __DIR__ . '/../../..' . '/inc/lib/Core/Abstracts/RunableAbstract.php',
        'Mazloy\\Core\\Container' => __DIR__ . '/../../..' . '/inc/lib/Core/Container.php',
        'Mazloy\\Core\\CustomPostType' => __DIR__ . '/../../..' . '/inc/lib/Core/CustomPostType.php',
        'Mazloy\\Core\\CustomTaxonomy' => __DIR__ . '/../../..' . '/inc/lib/Core/CustomTaxonomy.php',
        'Mazloy\\Core\\DotNotation' => __DIR__ . '/../../..' . '/inc/lib/Core/DotNotation.php',
        'Mazloy\\Core\\Exceptions\\Exception' => __DIR__ . '/../../..' . '/inc/lib/Core/Exceptions/Exception.php',
        'Mazloy\\Core\\InlineCss' => __DIR__ . '/../../..' . '/inc/lib/Core/InlineCss.php',
        'Mazloy\\Core\\InlineCss\\Css' => __DIR__ . '/../../..' . '/inc/lib/Core/InlineCss/Css.php',
        'Mazloy\\Core\\InlineCss\\RuleCollection' => __DIR__ . '/../../..' . '/inc/lib/Core/InlineCss/RuleCollection.php',
        'Mazloy\\Core\\InlineCss\\RuleSet' => __DIR__ . '/../../..' . '/inc/lib/Core/InlineCss/RuleSet.php',
        'Mazloy\\Core\\InlineCss\\Rules' => __DIR__ . '/../../..' . '/inc/lib/Core/InlineCss/Rules.php',
        'Mazloy\\Core\\JsonConfig' => __DIR__ . '/../../..' . '/inc/lib/Core/JsonConfig.php',
        'Mazloy\\Core\\Loader' => __DIR__ . '/../../..' . '/inc/lib/Core/Loader.php',
        'Mazloy\\Core\\Markup' => __DIR__ . '/../../..' . '/inc/lib/Core/Markup.php',
        'Mazloy\\Core\\QuickMetabox' => __DIR__ . '/../../..' . '/inc/lib/Core/QuickMetabox.php',
        'Mazloy\\Core\\Traits\\SingletonTrait' => __DIR__ . '/../../..' . '/inc/lib/Core/Traits/SingletonTrait.php',
        'Mazloy\\Core\\WmObject' => __DIR__ . '/../../..' . '/inc/lib/Core/WmObject.php',
        'Mazloy\\Metaboxes\\PageHeaderMetabox' => __DIR__ . '/../../..' . '/inc/lib/Metaboxes/PageHeaderMetabox.php',
        'Mazloy\\Metaboxes\\PageRowsMetabox' => __DIR__ . '/../../..' . '/inc/lib/Metaboxes/PageRowsMetabox.php',
        'Mazloy\\PostTypes\\HomePageSlider' => __DIR__ . '/../../..' . '/inc/lib/PostTypes/HomePageSlider.php',
        'Mazloy\\PostTypes\\Slider' => __DIR__ . '/../../..' . '/inc/lib/PostTypes/Slider.php',
        'Mazloy\\Setup\\JsonConfig' => __DIR__ . '/../../..' . '/inc/lib/Setup/JsonConfig.php',
        'Mazloy\\Setup\\ScriptsAndStyles' => __DIR__ . '/../../..' . '/inc/lib/Setup/ScriptsAndStyles.php',
        'Mazloy\\Setup\\TemplateSupport' => __DIR__ . '/../../..' . '/inc/lib/Setup/TemplateSupport.php',
        'Mazloy\\Setup\\ThemeSupport' => __DIR__ . '/../../..' . '/inc/lib/Setup/ThemeSupport.php',
        'Mazloy\\Template\\BodyClass' => __DIR__ . '/../../..' . '/inc/lib/Template/BodyClass.php',
        'Mazloy\\Template\\Overrides' => __DIR__ . '/../../..' . '/inc/lib/Template/Overrides.php',
        'Mazloy\\Template\\Template' => __DIR__ . '/../../..' . '/inc/lib/Template/Template.php',
        'Mazloy\\Template\\Wrapper' => __DIR__ . '/../../..' . '/inc/lib/Template/Wrapper.php',
        'Mazloy\\Theme' => __DIR__ . '/../../..' . '/inc/lib/Theme.php',
        'Mazloy\\Walkers\\CopyrightMenu' => __DIR__ . '/../../..' . '/inc/lib/Walkers/CopyrightMenu.php',
        'Mazloy\\Walkers\\Menu' => __DIR__ . '/../../..' . '/inc/lib/Walkers/Menu.php',
        'Mazloy\\Walkers\\SidebarMenu' => __DIR__ . '/../../..' . '/inc/lib/Walkers/SidebarMenu.php',
        'Mazloy\\Walkers\\TopBarMenu' => __DIR__ . '/../../..' . '/inc/lib/Walkers/TopBarMenu.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit8749344a8782a77924a3c8640c3894fc::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit8749344a8782a77924a3c8640c3894fc::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInit8749344a8782a77924a3c8640c3894fc::$classMap;

        }, null, ClassLoader::class);
    }
}
