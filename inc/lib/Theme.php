<?php
namespace Mazloy;

use Mazloy\Admin\PostsTables;
use Mazloy\Admin\Options;
use Mazloy\Core\Container;
use Mazloy\Core\InlineCss;
use Mazloy\Metaboxes\PageHeaderMetabox;
use Mazloy\Metaboxes\PageRowsMetabox;
use Mazloy\PostTypes\HomePageSlider;
use Mazloy\PostTypes\Slider;
use Mazloy\Setup\JsonConfig;
use Mazloy\Core\Loader;
use Mazloy\Core\Traits\SingletonTrait;
use Mazloy\Setup\ScriptsAndStyles;
use Mazloy\Setup\TemplateSupport;
use Mazloy\Setup\ThemeSupport;
use Mazloy\Template\BodyClass;
use Mazloy\Template\Overrides;
use Mazloy\Template\Template;

class Theme
{
	use SingletonTrait;

	/**
	 * Flag used to check for singleton instance
	 * @var bool
	 */
	protected $started = false;

	/**
	 * Used to store the config.json file wrapped in a JsonConfig object
	 * @var JsonConfig
	 */
	protected $config;

	/**
	 * Storage for all
	 * @var Container
	 */
	protected $container;

	/**
	 * Used to enqueue actions and filters for the theme.
	 * @var Loader
	 */
	protected $loader;

	protected function __construct()
	{
		$_theme_dir = trailingslashit(get_stylesheet_directory_uri());
		$_theme_path = trailingslashit(realpath(__DIR__ . '/../../'));
		$this->config = new JsonConfig($_theme_path . 'package.json');
		$_dev = file_exists($_theme_path . '.development');
		$this->container = new Container([

			// General
			// --------------------------------------------------------------------------

			'debug' => $this->config['debug'],
			'vendors' => [
				'path' => trailingslashit($_theme_path . 'inc/vendor')
			],
			'theme' => [
				'name'       => $this->config['theme']['name'],
				'short_name' => $this->config['theme']['short_name'],
				'directory'  => $_theme_dir,
				'path'       => $_theme_path,
				'version'    => $this->config['version'],
			],

			'env' => [
				'production'  => !$_dev,
				'development' => $_dev
			],

			'textdomain' => $this->config['theme']['short_name'],

			'google_api_key' => 'AIzaSyDG0RRww01LTeYfrELouaT3HAo1-9UW6aQ',

			// Asset paths and directories
			// --------------------------------------------------------------------------

			'assets' => [
				'css'          => trailingslashit($_theme_dir  . 'assets/styles'),
				'css_path'     => trailingslashit($_theme_path . 'assets/styles'),
				'styles'       => trailingslashit($_theme_dir  . 'assets/styles'),
				'styles_path'  => trailingslashit($_theme_path . 'assets/styles'),

				'js'           => trailingslashit($_theme_dir  . 'assets/scripts'),
				'js_path'      => trailingslashit($_theme_path . 'assets/scripts'),
				'scripts'      => trailingslashit($_theme_dir  . 'assets/scripts'),
				'scripts_path' => trailingslashit($_theme_path . 'assets/scripts'),

				'images'       => trailingslashit($_theme_dir  . 'assets/images'),
				'images_path'  => trailingslashit($_theme_path . 'assets/images'),
			],

			// Core and theme support
			// --------------------------------------------------------------------------

			'loader' => function () {
				return new Loader();
			},

			'template_support' => function (Container &$c) {
				return new TemplateSupport($c);
			},

			'template_overrides' => function(Container &$c) {
				return new Overrides($c);
			},

			'theme_support' => function (Container &$c) {
				return new ThemeSupport($c);
			},

			'admin_extras' => function (Container &$c) {
				return new PostsTables($c);
			},

			// Stylesheets and Script registration
			// --------------------------------------------------------------------------

			'front_end_assets' => function (Container &$c) {
				$prod = $c->get('env.production');
				$assets = new ScriptsAndStyles($c);

				// Stylesheets

				$vendorCss = [];
				if(file_exists($c->get('theme.path') . 'assets/styles/vendor' . ($prod ? '.min' : '') . '.css')) {
					$assets->style('vendor-styles', $c->get('assets.styles') . 'vendor' . ($prod ? '.min' : '') . '.css');
					$vendorCss[] = $assets->prefix('vendor-styles');
				}

				$assets->style('google-fonts', '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900,900i');
				$vendorCss[] = $assets->prefix('google-fonts');

				$assets->style('main', $c->get('assets.css') . 'app' . ($prod ? '.min' : '') . '.css', $vendorCss);

				$vendorJs = ['jquery'];
				if(file_exists($c->get('theme.path') . 'assets/scripts/vendor' . ($prod ? '.min' : '') . '.js')) {
					$assets->script('vendor-scripts', $c->get('assets.scripts') . 'vendor' . ($prod ? '.min' : '') . '.js', ['jquery']);
					$vendorJs[] = $assets->prefix('vendor-scripts');
				}

				$assets->script('slick', $c->get('assets.scripts') . 'vendors/slick/slick' . ($prod ?  '.min' : '') . '.js');
				$vendorJs[] = $assets->prefix('slick');

				// JavaScripts

				$mainJSFile = 'app' . ($prod ? '.min' : '') . '.js';
				$assets->script('main', $c->get('assets.js') . $mainJSFile, $vendorJs, $assets->version($c->get('assets.js_path') . $mainJSFile));
				$assets->script('main', $c->get('assets.js') . 'app' . ($prod ? '.min' : '') . '.js', $vendorJs);
				$assets->localize('main', strtoupper($c->get('theme.short_name')), [
					'debug' => !!($c->get('debug')),
					'short_name' => $c->get('theme.short_name'),
					'base_url' => get_home_url(),
					'rest_root' => esc_url_raw(rest_url('wp/v2/')),
					'nonce' => wp_create_nonce('wp_rest'),
				]);

				return $assets;
			},

			'auto_generated_css' => function(Container &$c) {
				return new InlineCss($c);
			},

			// Post Types
			// --------------------------------------------------------------------------

			'post_type_wm-slide' => function(Container &$c) {
				return new Slider($c);
			},

			'post_type_wm-home-slide' => function(Container &$c) {
				return new HomePageSlider($c);
			},

			// Metaboxes
			// --------------------------------------------------------------------------

			'metabox_page_rows' => function(Container &$c) {
				return new PageRowsMetabox($c);
			},

			'metabox_page_header' => function(Container &$c) {
				return new PageHeaderMetabox($c);
			},

			// Options and added functionality to the theme
			// --------------------------------------------------------------------------

			'extra_body_classes' => function(Container &$c) {
				return new BodyClass($c);
			},

			'admin_options' => function(Container &$c) {
				return new Options($c);
			},

			'admin_assets' => function(Container &$c) {
				$prod = $c->get('env.production');
				$assets = new ScriptsAndStyles($c);
				$assets->admin = true;

				$assets->style('main', $c->get('assets.css') . 'admin' . ($prod ? '.min' : '') . '.css', []);
				$assets->script('main', $c->get('assets.js') . 'admin' . ($prod ? '.min' : '') . '.js', ['jquery']);

				$assets->localize('main', strtoupper($c->get('theme.short_name')), [
					'debug' => (bool) $c->get('debug'),
					'short_name' => $c->get('theme.short_name'),
					'base_url' => get_home_url(),
				]);

				return $assets;
			},
		]);
	}

	/**
	 * Runs the app
	 */
	protected function _run()
	{
		foreach ($this->container->getRunables() as $name => $runable) {
			if ($name != 'loader')
				$runable->run();
		}

		$this->loader()->run();
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// API
	//
	// Methods here are used throughout the theme. You can use these methods
	// by calling mazloy()->filter() or mazloy('theme.path') which is the same as
	// Theme::getInstance()->container()->get('theme.path'). Passing a string
	// to the mazloy() wrapper function returns an object from the container while
	// using the method pointer `->` returns one of the following methods.
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * @param string $offset
	 * @return mixed|null
	 */
	public function get($offset)
	{
		return $this->container->get($offset);
	}

	/**
	 * Returns the container
	 * @return Container
	 */
	public function container()
	{
		return $this->container;
	}

	/**
	 * Returns the loader
	 * @return Loader
	 */
	public function loader()
	{
		return $this->container->get('loader');
	}

	/**
	 * Returns the wrapped template
	 * @param string $layout
	 * @return Template
	 */
	public function template($layout = 'base')
	{
		return Template::getInstance($layout);
	}

	/**
	 * Adds a script to the theme.
	 * @param string $handle
	 * @param string $source
	 * @param array|string $dependencies
	 * @param null|int|string $version
	 * @param string $screen
	 * @param bool $admin
	 * @return mixed
	 */
	public function style($handle, $source, $dependencies = [], $version = null, $screen = 'all', $admin = false)
	{
		return self::getInstance()->get($admin ? 'admin_assets' : 'front_end_assets')
		           ->style($handle, $source, $dependencies, $version, $screen);
	}

	/**
	 * Adds a script to the theme
	 * @param string $handle
	 * @param string $source
	 * @param array|string $dependencies
	 * @param null|string|int $version
	 * @param bool $footer
	 * @param bool $admin
	 * @return mixed
	 */
	public function script($handle, $source, $dependencies = [], $version = null, $footer = true, $admin = false)
	{
		return self::getInstance()->get($admin ? 'admin_assets' : 'front_end_assets')
		           ->script($handle, $source, $dependencies, $version, $footer);
	}

	/**
	 * @param string $handle
	 * @param array|string|object $object
	 * @param array $data
	 * @param bool $admin
	 */
	public function localize($handle, $object, $data = [], $admin = false)
	{
		return self::getInstance()->get($admin ? 'admin_assets' : 'front_end_assets')
		           ->localize($handle, $object, $data);
	}

	/**
	 * Adds an action to the theme
	 * @param string $hook
	 * @param callable $callback
	 * @param int $priority
	 * @param int $args
	 * @return $this
	 */
	public function action($hook, callable $callback, $priority = 10, $args = 1)
	{
		$instance = self::getInstance();
		$instance->loader()->addAction($hook, $callback, $priority, $args);
		return $instance;
	}

	/**
	 * Adds a filter
	 * @param string $hook
	 * @param callable $callback
	 * @param int $priority
	 * @param int $args
	 * @return $this
	 */
	public function filter($hook, callable $callback, $priority = 10, $args = 1)
	{
		$instance = self::getInstance();
		$instance->loader()->addFilter($hook, $callback, $priority, $args);
		return $instance;
	}

	/**
	 * Returns prefixed string with short namespace
	 * @param string $name
	 * @return string
	 */
	public function prefix($name ='')
	{
		return $this->get('front_end_assets')->prefix($name);
	}

	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Static API
	//
	// Primarily used to start and stop the theme
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	/**
	 * Starts the theme
	 * @return Theme
	 */
	public static function start()
	{
		$instance = self::getInstance();

		if (!$instance->started) {
			$instance->_run();
			$instance->started = true;
		}

		return $instance;
	}

	/**
	 * Kills the application and redirects to a wordpress error page with a message
	 * @param string $error
	 * @param string $subtitle
	 * @param string $title
	 */
	public static function stop($error, $subtitle = '', $title = '')
	{
		$title = $title ?: __('Mazloy Theme - Error', self::getInstance()->get('textdomain'));
		$message = "<h1>{$title}";

		if ($subtitle)
			$message .= "<br><small>{$subtitle}</small>";

		$message .= "</h1>";
		$message .= "<p>{$error}</p>";

		wp_die($message);
	}
}