<?php

namespace Mazloy\Core;

use Mazloy\Core\Abstracts\RunableAbstract;

class CustomTaxonomy extends RunableAbstract {
	public $slug;
	public $single;
	public $plural;
	public $args = [];
	public $post_type;

	public function addTaxonomy() {
		register_taxonomy( $this->slug, [ $this->post_type ], wp_parse_args( $this->args, [
			'hierarchical'      => true,
			'labels'            => [
				'name'              => sprintf( _x( '%s', 'taxonomy general name', $this->get( 'textdomain' ) ), $this->plural ),
				'singular_name'     => sprintf( _x( '%s', 'taxonomy singular name', $this->get( 'textdomain' ) ), $this->single ),
				'search_items'      => sprintf( __( 'Search %s', $this->get( 'textdomain' ) ), $this->plural ),
				'all_items'         => sprintf( __( 'All %s', $this->get( 'textdomain' ) ), $this->plural ),
				'parent_item'       => sprintf( __( 'Parent %s', $this->get( 'textdomain' ) ), $this->single ),
				'parent_item_colon' => sprintf( __( 'Parent %s:', $this->get( 'textdomain' ) ), $this->single ),
				'edit_item'         => sprintf( __( 'Edit %s', $this->get( 'textdomain' ) ), $this->single ),
				'update_item'       => sprintf( __( 'Update %s', $this->get( 'textdomain' ) ), $this->single ),
				'add_new_item'      => sprintf( __( 'Add New %s', $this->get( 'textdomain' ) ), $this->single ),
				'new_item_name'     => sprintf( __( 'New %s Name', $this->get( 'textdomain' ) ), $this->single ),
				'menu_name'         => sprintf( __( '%s', $this->get( 'textdomain' ) ), $this->single ),
			],
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => [ 'slug' => 'genre' ],
		] ) );
	}

	public function run() {
		$this->loader()->addAction( 'init', [ $this, 'addTaxonomy' ] );
	}
}