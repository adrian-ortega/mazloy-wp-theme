<?php

namespace Mazloy\Core;

class WmObject
{
	/**
	 * @var array
	 */
	public $data = [];

	/**
	 * @param $path
	 *
	 * @return bool
	 */
	public function has( $path = '' ) {
		return DotNotation::exists($path, $this->data);
	}

	/**
	 * Returns out the data array
	 * @return array
	 */
	public function toArray() {
		return $this->data;
	}

	/**
	 * @param string $path
	 *
	 * @return mixed
	 */
	public function get( $path = '' ) {
		return DotNotation::parse($path, $this->data);
	}

	public function set( $offset, $value ) {
		$this->data[$offset] = $value;
	}
}