<?php

namespace Mazloy\Core;

class Markup
{
	/**
	 * Returns HTML for a specific element tag
	 * @param string $tag
	 * @param array $attributes
	 * @param bool $content
	 * @return string
	 */
	public static function tag($tag = 'div', $attributes = [], $content = false)
	{
		$out = "<{$tag}";
		$out .= self::attributes($attributes);
		$out .= $content !== false ? ">{$content}</{$tag}>" : '/>';
		return $out;
	}

	/**
	 * Returns attributes for an HTML ELement
	 * @param array $attributes
	 * @return string
	 */
	public static function attributes($attributes = [])
	{
		if(empty($attributes))
			return '';

		$out = [];
		foreach($attributes as $attribute => $value) {
			if ( $attribute == 'style' ) {
				$_value = [];
				foreach ( $value as $property => $propertyValue ) {
					$_value[] = "{$property}:{$propertyValue}";
				}
				$value = implode( ';', $_value );
			} else if ( is_array( $value ) ) {
				$value = implode( ' ', $value );
			}


			$out[] = "{$attribute}=\"{$value}\"";
		}
		return ' ' . implode(' ', $out);
	}

	/**
	 * Returns a WordPress Dashicon
	 * @url https://developer.wordpress.org/resource/dashicons
	 * @param string $which
	 * @return string
	 */
	public static function icon( $which = 'star-filled' ) {
		return Markup::tag('span', [
			'class' => [
				'dashicons',
				"dashicons-{$which}"
			]
		], '');
	}

	/**
	 * Returns out a single HTML comment line
	 * @param string $comment
	 *
	 * @return string
	 */
	public static function comment( $comment = '' ) {
		return !empty($comment) ? "\n<!-- {$comment} -->\n" : '';
	}

	/**
	 * Returns a class attribute set
	 * @param array $classes
	 *
	 * @return string
	 */
	public static function classes( $classes = array() ) {
		return self::attributes(['class' => (array) $classes]);
	}
}