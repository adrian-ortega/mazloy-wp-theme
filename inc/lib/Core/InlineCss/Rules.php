<?php

namespace Mazloy\Core\InlineCss;

class Rules
{
	/**
	 * Array of strings as css selectors
	 * @var array
	 */
	public $selectors = [];

	/**
	 * @var array|RuleSet[]
	 */
	public $rules = [];

	/**
	 * Minifier flag for the output
	 * @var bool
	 */
	public $minify = false;

	public function __construct( $config = array(), $minify = false ) {
		$this->minify = $minify;

		if ( isset( $config['selectors'] ) && ! empty( $config['selectors'] ) ) {
			$this->selectors = (array) $config['selectors'];
		}

		if ( isset( $config['rules'] ) && ! empty( $config['rules'] ) ) {
			foreach ( $config['rules'] as $property => $value ) {
				$this->rule( $property, $value );
			}
		}
	}

	/**
	 * Adds a property and value to the rule set
	 * @param string $property
	 * @param string $propertyValue
	 *
	 * @return $this
	 */
	public function rule( $property = '', $propertyValue = '' ) {
		$this->rules[] = new RuleSet( $property, $propertyValue, $this->minify );

		return $this;
	}

	/**
	 * Adds a selector to the rule set
	 * @param string $selector
	 *
	 * @return $this
	 */
	public function selector( $selector = '' ) {
		$this->selectors[] = $selector;

		return $this;
	}

	/**
	 * Returns the CSS output string
	 * @return string
	 */
	public function getOutput() {
		$s = $this->minify ? '' : ' ';
		$b = $this->minify ? '' : "\n";
		$t = $this->minify ? '' : "\t";

		$output = implode( ",{$b}", $this->selectors ) . "{$s}{{$b}";
		foreach ( $this->rules as $rule ) {
			$output .= $t . $rule->getOutput();
		}
		$output .= "}{$b}{$b}";

		return $output;
	}

	/**
	 * Print wrapper for $this->getOutput()
	 * @return void
	 */
	public function output() {
		echo $this->getOutput();
	}
}