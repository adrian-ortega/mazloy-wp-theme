<?php

namespace Mazloy\Core\InlineCss;

class Css
{
	/**
	 * @param string $hex
	 * @param int $opacity
	 *
	 * @return string
	 */
	public static function rgba( $hex = "#000000", $opacity = 1 ) {
		$color = self::hexToRgb($hex, true);
		return "rgba({$color},$opacity)";
	}

	/**
	 * @param string $hexStr
	 * @param bool $returnAsString
	 * @param string $delimiter
	 *
	 * @return array|bool|string
	 */
	public static function hexToRgb($hexStr, $returnAsString = false, $delimiter = ',') {
		$hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr);
		$rgb = [];

		// If a proper hex code, convert using bitwise operation. No overhead... faster
		if (strlen($hexStr) == 6) {
			$colorVal = hexdec($hexStr);
			$rgb['red'] = 0xFF & ($colorVal >> 0x10);
			$rgb['green'] = 0xFF & ($colorVal >> 0x8);
			$rgb['blue'] = 0xFF & $colorVal;
		}

		// if shorthand notation, need some string manipulations
		elseif (strlen($hexStr) == 3) {
			$rgb['red']   = hexdec( str_repeat( substr( $hexStr, 0, 1 ), 2 ) );
			$rgb['green'] = hexdec( str_repeat( substr( $hexStr, 1, 1 ), 2 ) );
			$rgb['blue']  = hexdec( str_repeat( substr( $hexStr, 2, 1 ), 2 ) );
		}

		// Invalid hex color code
		else {
			return false;
		}

		return $returnAsString ? implode($delimiter, $rgb) : $rgb;
	}
}