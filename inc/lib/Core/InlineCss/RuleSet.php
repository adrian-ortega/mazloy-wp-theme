<?php

namespace Mazloy\Core\InlineCss;

class RuleSet
{
	/**
	 * The CSS property
	 * @var string
	 */
	public $property;

	/**
	 * The rule set value
	 * @var string
	 */
	public $value;

	/**
	 * @var bool
	 */
	protected $minify = false;

	/**
	 * RuleSet constructor.
	 *
	 * @param string $property
	 * @param string $value
	 * @param bool $minify
	 */
	public function __construct($property = '', $value = '', $minify = false) {
		$this->property = $property;
		$this->value    = $value;
		$this->minify   = $minify;
	}

	/**
	 * Returns the output of this string
	 * @return string
	 */
	public function getOutput(  ) {
		$s = $this->minify ? '' : ' ';
		$b = $this->minify ? '' : "\n";

		return "{$this->property}{$s}:{$s}{$this->value};{$b}";
	}

	/**
	 * Print Wrapper for $this->getOutput()
	 */
	public function output() {
		echo $this->getOutput();
	}
}