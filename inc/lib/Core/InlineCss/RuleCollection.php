<?php

namespace Mazloy\Core\InlineCss;

use Mazloy\Core\WmObject;

class RuleCollection extends WmObject
{
	/**
	 * RuleCollection constructor.
	 *
	 * @param array $rules
	 * @param bool $minify
	 */
	public function __construct( $rules = array() , $minify = false) {
		$this->populate($rules, $minify);
	}

	/**
	 * Populates the collection with css rules
	 * @param array $rules
	 * @param bool $minify
	 */
	public function populate( $rules = array(), $minify = false ) {
		$collection = [];
		foreach($rules as $rule) {
			$collection[] = new Rules($rule, $minify);
		}

		$this->set('collection', $collection);
	}

	/**
	 * Returns the output of this collection
	 * @return string
	 */
	public function getOutput() {
		$output = '';

		/**
		 * @var Rules $rule
		 */
		foreach($this->get('collection') as $rule)
			$output .= $rule->getOutput();

		return $output;
	}

	/**
	 * Print Wrapper for $this->getOutput()
	 */
	public function output() {
		echo $this->getOutput();
	}
}