<?php

namespace Mazloy\Core;

use Mazloy\Core\Abstracts\MetaboxAbstract;
use Mazloy\Core\Abstracts\RunableAbstract;
use Mazloy\Core\Exceptions\Exception;

abstract class CustomPostType extends RunableAbstract
{
	/**
	 * @var null|string
	 */
	public $post_type = null;

	/**
	 * @var array
	 */
	public $post_args = [];

	/**
	 * CustomPostType constructor.
	 *
	 * @param Container $container
	 */
	public function __construct( Container &$container ) {
		parent::__construct($container);
		$this->setup();
	}

	/**
	 * @return void
	 */
	abstract protected function setup();

	/**
	 * Adds labels bases on a single and plural label
	 *
	 * @param string $single
	 * @param string $plural
	 *
	 * @param array $overrides
	 *
	 * @return $this
	 */
	protected function labels($single = '', $plural = '', $overrides = []) {
		$this->post_args['labels'] = wp_parse_args($overrides,[
			'name'               => sprintf( _x( '%s', 'post type general name', $this->get( 'textdomain' ) ), $plural ),
			'singular_name'      => sprintf( _x( '%s', 'post type singular name', $this->get( 'textdomain' ) ), $single ),
			'menu_name'          => sprintf( _x( '%s', 'admin menu', $this->get( 'textdomain' ) ), $plural ),
			'name_admin_bar'     => sprintf( _x( '%s', 'add new on admin bar', $this->get( 'textdomain' ) ), $single ),
			'add_new'            => _x( 'Add New', $this->post_type, $this->get( 'textdomain' ) ),
			'add_new_item'       => sprintf( __( 'Add New %s', $this->get( 'textdomain' ) ), $single ),
			'new_item'           => sprintf( __( 'New %s', $this->get( 'textdomain' ) ), $single ),
			'edit_item'          => sprintf( __( 'Edit %s', $this->get( 'textdomain' ) ), $single ),
			'view_item'          => sprintf( __( 'View %s', $this->get( 'textdomain' ) ), $single ),
			'all_items'          => sprintf( __( 'All %s', $this->get( 'textdomain' ) ), $plural ),
			'search_items'       => sprintf( __( 'Search %s', $this->get( 'textdomain' ) ), $plural ),
			'parent_item_colon'  => sprintf( __( 'Parent %s:', $this->get( 'textdomain' ) ), $plural ),
			'not_found'          => sprintf( __( 'No %s found.', $this->get( 'textdomain' ) ), strtolower( $plural ) ),
			'not_found_in_trash' => sprintf( __( 'No %s found in Trash.', $this->get( 'textdomain' ) ), strtolower( $plural ) )
		]);

		return $this;
	}

	/**
	 * The public callback added through the action hook
	 * @throws Exception
	 */
	public function add_post_type() {
		if($this->post_type === null)
			throw new Exception('Missing post type');

		register_post_type($this->post_type, $this->post_args);
	}

	/**
	 * Adds custom taxonomy to a post type
	 *
	 * @param $slug
	 * @param string $single
	 * @param string $plural
	 * @param array $args
	 *
	 * @return $this
	 */
	protected function taxonomy($slug, $single, $plural, $args = []) {
		$name = "{$this->post_type}_tax_{$slug}";

		$this->container->set($name, function(Container &$container) use($slug, $single, $plural, $args) {
			$taxonomy            = new CustomTaxonomy( $container );
			$taxonomy->post_type = $this->post_type;
			$taxonomy->slug      = $slug;
			$taxonomy->single    = $single;
			$taxonomy->plural    = $plural;
			$taxonomy->args      = $args;

			return $taxonomy;
		});

		// force get
		$this->container->get($name);

		return $this;
	}

	/**
	 * Adds a QuickMetabox
	 * @param null|string|array $name
	 * @param array $args
	 *
	 * @return $this
	 */
	protected function metabox($name = null, $args = []) {
		if(is_array($name)) {
			$args = $name;
			$name = isset($args['name']) ? $args['name'] : null;
		}

		$metabox = new QuickMetabox($this->container);
		$metabox->boot(array_merge([
			'post_type'       => $this->post_type,
			'priority'        => 'high',
		], $args))->enqueue($name);

		// force queue
		$this->container->get($metabox->get_queue_name($name));

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function run() {
		$this->loader()->addAction('init', [$this, 'add_post_type']);
	}
}