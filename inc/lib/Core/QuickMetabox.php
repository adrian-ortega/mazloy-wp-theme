<?php
/**
 * NO STEP ON SNEK:
 *
 * This abstract class, unlike others within this theme, uses the snake_casing
 * naming convention: https://en.wikipedia.org/wiki/Snake_case like most WordPress
 * core classes. Everything else will use the camelCasing naming convention.
 */

namespace Mazloy\Core;

use Mazloy\Core\Abstracts\MetaboxAbstract;

class QuickMetabox extends MetaboxAbstract {
	/**
	 * Arguments for setup
	 * @var array
	 */
	public $args;

	/**
	 * The registered fields for the metabox
	 * @var array
	 */
	protected $fields;

	/**
	 * name of the storage key to use with the container for registered metaboxes
	 * @var string
	 */
	protected $storage_name = 'quick_metabox';

	/**
	 * Flag to check if the current metabox has been registered with the container
	 * @var bool
	 */
	public $registered = false;

	/**
	 * @inheritdoc
	 */
	public function scripts() {
		wp_enqueue_media();
		wp_enqueue_style('wp-color-picker');
	}

	/**
	 * QuickMetabox constructor overrides abstracts param requirements.
	 */
	public function setup() {
		// Use this to extend this class, but if called as new, use boot
	}

	/**
	 * @inheritdoc
	 */
	public function boot( $args = [] ) {
		// Grab the already registered quick metaboxes so we can add to the array. if empty,
		// create new array
		$metaboxes = $this->registeredMetaboxes();

		// Defaults
		$this->args = wp_parse_args( $args, [
			'name'            => null,
			'post_type'       => null,
			'context'         => null,
			'priority'        => null,
			'callback'        => null,

			// unique to Quick metabox
			'metakey'         => 'undefined',
			'use_single_keys' => false,

			'fields' => [
				'test' => $this->parseField( [
					'type'        => 'text',
					'label'       => 'Label One',
					'default'     => 'Default Value',
					'description' => 'Some sort of description'
				] )
			],
		] );

		// Just in case the wrong key name is passed...
		if ( isset( $this->args['meta_key'] ) ) {
			$this->args['metakey'] = $this->args['meta_key'];
			unset( $this->args['meta_key'] );
		}

		if ( ! ( $this->registered = isset( $metaboxes[ $this->args['metakey'] ] ) && $metaboxes[ $this->args['metakey'] ]['registered'] ) ) {

			// Go through each property and set them as part of this
			// class instance
			foreach ( $this->args as $property => $value ) {
				if ( $value === null ) {
					continue;
				} elseif ( method_exists( $this, $property ) ) {
					$this->{$property}( $value );
				} elseif ( property_exists( $this, $property ) ) {
					$this->{$property} = $value;
				}

				if ( $property == 'fields' ) {
					foreach ( $value as $field_key => $field ) {
						if(strpos($field_key, '__') !== 0)
							$this->defaults[ $field_key ] = isset( $field['default'] ) ? $field['default'] : '';
					}
				}
			}

			$this->args['registered'] = $this->registered = true;

			// Save the instance
			if ( ! isset( $metaboxes[ $this->args['post_type'] ] ) ) {
				$metaboxes[ $this->args['post_type'] ] = [];
			}

			$metaboxes[ $this->args['post_type'] ][] = $this->args['metakey'];
			$this->container['quick_metaboxes']      = $metaboxes;
		}

		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function display( \WP_Post $post ) {
		$meta = $this->get_meta( null, $post );
		$html = '';

		$html .= do_action( $this->eventName( 'display_before' ), $this->args, $meta );
		foreach ( $this->fields as $field_key => $field ) {
			$html .= $this->doField( $field_key, $field, $meta );
		}
		$html .= do_action( $this->eventName( 'display_after' ), $this->args, $meta );

		echo Markup::tag( 'div', [
			'data-required-prefix' => $this->metakey,
			'class' => 'mazloy-quick-metabox',
		], $html );
	}

	/**
	 * Prints out a field depending on arguments
	 *
	 * @param $field_key
	 * @param $field
	 * @param array $meta
	 *
	 * @return mixed
	 */
	protected function doField( $field_key = '', $field = [], $meta = [] ) {
		$field    = $this->parseField( $field );
		$callback = [ $this, 'display_text_field' ];

		if ( isset( $field['callback'] ) && is_callable( $field['callback'] ) ) {
			$callback = $field['callback'];
		} elseif ( ( $method = sprintf( 'display_%s_field', $field['type'] ) ) && method_exists( $this, $method ) ) {
			$callback = [ $this, $method ];
		}

		$field_value = DotNotation::parse(implode('.', (array) $field_key), $meta);

		return call_user_func_array( $callback, [ $field_value, $field_key, $field ] );
	}

	//////////////////////////////////////////////////
	//  Local helpers
	//////////////////////////////////////////////////

	/**
	 * Returns a prefixed event name for either a filter or action
	 *
	 * @param string $name
	 *
	 * @return string
	 */
	protected function eventName( $name = '' ) {
		return empty( $name ) ? '' : "mazloy.quick_metabox_{$this->metakey}_{$name}";
	}

	//////////////////////////////////////////////////
	//  Field display callbacks
	//////////////////////////////////////////////////

	/**
	 * Wraps a field with a div.field element
	 *
	 * @param string|array $field
	 * @param string $html
	 *
	 * @return string
	 */
	protected function display_field_wrap( $field, $html) {
		$attributes = [
			'class' => ['field']
		];

		if ( is_array( $field ) ) {
			if( isset($field['type'])) {
				if(is_array($field['type'])) {
					$attributes['class'] = array_merge( $attributes['class'], $field['type'] );
				} else {
					$attributes['class'][] = "field-{$field['type']}";
				}
			} else {
				$attributes['class'] = array_merge( $attributes['class'], $field );
			}

			if ( $field['required'] ) {
				$required = $field['required'];

				if(!is_array($required[0])) {
					$required = [$required];
				}

				$parsed = [];

				foreach($required as $set) {
					if ( in_array( $set[1], [ '=', 'equals', 'is' ] ) ) {
						$parsed[ $set[0] ] = "== '{$set[2]}'";
					} elseif ( in_array( $set[1], [ '!=', 'not', 'is not' ] ) ) {
						$parsed[ $set[0] ] = "!= '{$set[2]}'";
					} elseif ( in_array( $set[1], [ '>', 'greater than' ] ) ) {
						$parsed[ $set[0] ] = "> '{$set[2]}'";
					} elseif ( in_array( $set[1], [ '<', 'less than' ] ) ) {
						$parsed[ $set[0] ] = "< '{$set[2]}'";
					} elseif ( in_array( $set[1], [ '<=', 'less than or equal' ] ) ) {
						$parsed[ $set[0] ] = "<= '{$set[2]}'";
					} elseif ( in_array( $set[1], [ '>=', 'greater than or equal' ] ) ) {
						$parsed[ $set[0] ] = "<= '{$set[2]}'";
					} elseif(count($set) == 2) {
						$parsed[$set[0]] = "== '{$set[1]}'";
					}
				}
				$attributes['data-required'] = htmlspecialchars(json_encode($parsed), ENT_QUOTES, 'UTF-8');
			}
		} else {
			$attributes['class'][] = "field-{$field}";
		}

		return Markup::tag( 'div', $attributes, $html );
	}

	/**
	 * Main field display adding a label and description
	 *
	 * @param array $args
	 * @param string $name
	 * @param string $field_html
	 *
	 * @return string
	 */
	protected function display_field( $args = [], $name = '', $field_html = '' ) {
		$html = '';
		if ( $args['label'] ) {
			$_label_html = Markup::tag( 'label', [
				'for'   => $this->get_html_id( $name ),
				'class' => [ 'field-label', 'field-label-' . $args['type'] ]
			], $args['label'] );

			if ( $args['sub_label'] || $args['sublabel'] ) {
				$_label_html .= Markup::tag( 'small', [
					'class' => [ 'field-sub-label', 'field-sub-label-' . $args['type'] ]
				], $args['sub_label'] ? $args['sub_label'] : $args['sublabel'] );
			}

			$html .= Markup::tag( 'div', [
				'class' => [ 'field-label-wrap', 'field-label-wrap-' . $args['type'] ]
			], $_label_html );
		}

		$_field_html = $field_html;

		if ( $args['description'] ) {
			$_field_html .= Markup::tag( 'p', [ 'class' => 'description' ], $args['description'] );
		}

		$html .= Markup::tag( 'div', [
			'class' => [ 'field-content-wrap', 'field-content-wrap-' . $args['type'] ]
		], $_field_html );

		return $this->display_field_wrap( $args, $html );
	}

	/**
	 * Text field display callback
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_text_field( $value = '', $name = '', $args = [] ) {
		return $this->display_field( $args, $name, Markup::tag( 'input', [
			'type'        => $args['type'],
			'id'          => call_user_func_array( [ $this, 'get_html_id' ], (array) $name ),
			'name'        => call_user_func_array( [ $this, 'get_input_name' ], (array) $name ),
			'placeholder' => isset( $args['placeholder'] ) ? $args['placeholder'] : '',
			'value'       => $value
		] ) );
	}

	/**
	 * Textarea display callback
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_textarea_field( $value = '', $name = '', $args = [] ) {
		return $this->display_field( $args, $name, Markup::tag( 'textarea', [
			'id'   => call_user_func_array( [ $this, 'get_html_id' ], (array) $name ),
			'name' => call_user_func_array( [ $this, 'get_input_name' ], (array) $name ),
		], $value ) );
	}

	/**
	 * Select dropdown display callback
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_select_field( $value = '', $name = '', $args = [] ) {
		$value   = (array) $value;
		$options = '';
		foreach ( $this->associativeOptions( $args['options'] ) as $option_value => $option_text ) {
			$attr = [ 'value' => $option_value ];

			if ( in_array( $option_value, $value ) ) {
				$attr['selected'] = 'selected';
			}

			$options .= Markup::tag( 'option', $attr, $option_text );
		}

		return $this->display_field( $args, $name, Markup::tag( 'select', [
			'id'   => call_user_func_array( [ $this, 'get_html_id' ], (array) $name ),
			'name' => call_user_func_array( [ $this, 'get_input_name' ], (array) $name ),
		], $options ) );
	}

	/**
	 * Single checkbox display callback
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_checkbox_field( $value = '', $name = '', $args = [] ) {
		$id = (array) $name;
		$name = (array) $name;
		if($args['type'] === 'radio') {
			array_pop($name);
		}

		$input_args = [
			'type'  => $args['type'],
			'id'    => call_user_func_array( [ $this, 'get_html_id' ], $id ),
			'name'  => call_user_func_array( [ $this, 'get_input_name' ], $name  ),
			'value' => $args['value']
		];

		if ( $value == $args['value'] ) {
			$input_args['checked'] = 'checked';
		}

		$checkbox_args = array_merge( [ 'for' => $input_args['id'] ], isset( $args['input_label'] ) ? $args['input_label'] : [] );
		$checkbox      = Markup::tag( 'label', $checkbox_args,
			Markup::tag( 'input', $input_args ) .
			Markup::tag( 'span', [], $args['label'] )
		);

		return isset( $args['no_wrap'] ) && $args['no_wrap'] ? $checkbox : $this->display_field_wrap( 'checkbox', $checkbox );
	}

	/**
	 * Multiple checkbox display callback
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_checkboxes_field( $value = '', $name = '', $args = [] ) {
		$i     = 0;
		$html  = '';
		$value = (array) $value;
		$name  = (array) $name;
		$type  = 'checkbox';

		if ( $args['type'] == 'radios' ) {
			$type = 'radio';
		}


		foreach ( $this->associativeOptions( $args['options'] ) as $checkbox_value => $checkbox_label ) {
			$html .= $this->display_checkbox_field( $value, array_merge($name, [$i++]), array_merge( $args, [
				'type'    => $type,
				'label'   => $checkbox_label,
				'no_wrap' => true,
			] ) );
		}

		return $this->display_field_wrap( 'checkboxes', $html );
	}

	/**
	 * Single radio display callback
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_radio_field( $value = '', $name = '', $args = [] ) {
		return $this->display_checkbox_field( $value, $name, $args );
	}

	/**
	 * Multi radio display callback
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_radios_field( $value = '', $name = '', $args = [] ) {
		return $this->display_checkboxes_field( $value, $name, $args );
	}

	/**
	 * Displays a multiple button option
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_button_option_field( $value = '', $name = '', $args = [] ) {
		$html  = '';
		$i     = 0;
		$total = count( $args['options'] );
		$name = (array) $name;

		foreach ( $args['options'] as $button_key => $button_label ) {
			$button_class = [ 'button' ];

			if ( $i ++ === 0 ) {
				$button_class[] = 'button-first';
			}

			if ( $i === $total ) {
				$button_class[] = 'button-last';
			}

			if ( $button_key == $value ) {
				$button_class[] = 'button-primary';
			}

			$html .= $this->display_checkbox_field( $value, array_merge($name, [$i]), [
				'type'        => 'radio',
				'label'       => $button_label,
				'no_wrap'     => true,
				'value'       => $button_key,
				'input_label' => [
					'class' => $button_class
				]
			] );
		}

		$html = Markup::tag( 'div', [ 'class' => [ 'button-options' ] ], $html );

		return $this->display_field( $args, $name, $html );
	}

	/**
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_switch_field( $value = '', $name = '', $args = [] ) {
		$html       = '';
		$input_args = [
			'type'  => 'checkbox',
			'id'   => call_user_func_array( [ $this, 'get_html_id' ], (array) $name ),
			'name' => call_user_func_array( [ $this, 'get_input_name' ], (array) $name ),
			'value' => 1,
		];

		if ( $value == 1 ) {
			$input_args['checked'] = 'checked';
		}

		$html .= Markup::tag( 'input', $input_args );
		$html .= Markup::tag( 'label', [
			'for'   => $this->get_html_id( $name )
		], '' );

		$html = Markup::tag( 'div', [ 'class' => 'button-switch' ], $html );

		return $this->display_field( $args, $name, $html );
	}

	/**
	 * Displays more than one field using the same syntax
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_fields_field( $value = '', $name = '', $args = [] ) {
		$cells = [];
		$value = (array) $value;
		$keys  = array_keys( $args['fields'] );

		foreach ( $args['fields'] as $field_key => $field ) {
			$keyIndex   = array_search( $field_key, $keys );
			$columnClass = [
				'field-column',
				"field-column-{$field['type']}"
			];

			if ( $keyIndex === 0 ) {
				$columnClass[] = 'field-column-first';
			}

			if ( count( $keys ) === ( $keyIndex - 1 ) ) {
				$columnClass[] = 'field-column-last';
			}

			if ( isset( $field['width'] ) ) {
				$columnClass[] = 'field-column-' . $field['width'];
			}

			$cells[] = Markup::tag( 'div', [
				'class' => $columnClass,
			], $this->doField( [$name, $field_key], $field, [$name => $value] ) );
		}

		return $this->display_field( $args, $name, Markup::tag( 'div', [
			'class' => 'field-columns'
		], implode( '', $cells )) );
	}

	/**
	 * Displays a set of fields that allow you to configure a button with text, url, style and color
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_button_field( $value = '', $name = '', $args = [] ) {
		$args['fields'] = [
			'text'  => [
				'type'      => 'text',
				'label'     => __( 'Text', $this->get( 'textdomain' ) ),
				'description' => sprintf( 'Example: %s', '<em>Learn More</em>' ),
				'width'     => 4
			],
			'url'   => [
				'type'      => 'url',
				'label'     => __( 'URL', $this->get( 'textdomain' ) ),
				'description' => sprintf( __( 'Example: %s', $this->get( 'textdomain' ) ), '<em>http://Mazloy.com</em>' ),
				'width'     => 4
			],
			'style' => [
				'type'      => 'select',
				'label'     => __( 'Style', $this->get( 'textdomain' ) ),
				'default'   => 'solid_color',
				'options'   => [
					'solid_color'        => 'Solid Color BG',
					'transparent_border' => 'Transparent w/ Border',
					'transparent_hover'  => 'Transparent w/ Solid BG Hover',
				],
				'width'     => 2
			],
			'color' => [
				'type'      => 'select',
				'label'     => __( 'Color', $this->get( 'textdomain' ) ),
				'default'   => 'solid_color',
				'options'   => [
					'primary' => 'Primary Color',
					'extra_1' => 'Extra Color #1',
					'extra_2' => 'Extra Color #2',
					'extra_3' => 'Extra Color #3',
				],
				'width'     => 2
			]
		];

		return $this->display_fields_field( $value, $name, $args );
	}

	/**
	 * Displays a set fields that return a top and left position for elements
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_element_position_field( $value = '', $name = '', $args = [] ) {
		$default_horz = isset($args['default']['horizontal']) ? $args['default']['horizontal'] : 'center';
		$default_vert = isset($args['default']['vertical']) ? $args['default']['vertical'] : 'center';

		$value = wp_parse_args($value, [
			'horizontal' => $default_horz,
			'vertical' => $default_vert,
		]);

		$args['fields'] = [
			'horizontal' => [
				'type'    => 'button_option',
				'label'   => __( 'Horizontal Alignment', $this->get( 'textdomain' ) ),
				'default' => $default_horz,
				'options' => [
					'left'   => 'Left',
					'center' => 'Centered',
					'right'  => 'Right'
				],
				'width' => 6
			],
			'vertical'   => [
				'type'    => 'button_option',
				'label'   => __( 'Vertical Alignment', $this->get( 'textdomain' ) ),
				'default' => $default_vert,
				'options' => [
					'top'    => 'Top',
					'center' => 'Centered',
					'bottom' => 'Bottom'
				],
				'width' => 6
			]
		];

		return $this->display_fields_field( $value, $name, $args );
	}

	/**
	 * Displays a note
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_note_field( $value = '', $name = '', $args = [] ) {
		$html = '';
		if ( $args['label'] ) {
			$sub_label = '';
			if ( $args['sub_label'] ) {
				$sub_label = Markup::tag( 'small', [ 'class' => 'sub-header' ], $args['sub_label'] );
			}

			$html .= Markup::tag( 'h3', [], $args['label'] . $sub_label );
		}

		if ( $args['description'] ) {
			$html .= apply_filters( 'the_content', $args['description'] );
		}

		return $this->display_field_wrap( $args, $html );
	}

	protected function display_range_field( $value = '', $name = '', $args = [] ) {
		$change_id = call_user_func_array( [ $this, 'get_html_id' ], array_merge((array) $name, ['output']));
		$min = isset($args['min']) ? $args['min'] : 1;
		$max = isset($args['max']) ? $args['max'] : 100;
		$step = isset($args['step']) ? $args['step'] : 1;
		return $this->display_field( $args, $name, Markup::tag( 'input', [
			'type'        => 'range',
			'id'          => call_user_func_array( [ $this, 'get_html_id' ], (array) $name ),
			'name'        => call_user_func_array( [ $this, 'get_input_name' ], (array) $name ),
			'placeholder' => isset( $args['placeholder'] ) ? $args['placeholder'] : '',
			'value'       => $value,
			'min'         => $min,
			'max'         => $max,
			'step'        => $step,
			'oninput'     => "document.getElementById('{$change_id}').innerText = this.value",
			'onchange'    => "document.getElementById('{$change_id}').innerText = this.value"
		] ) . Markup::tag('span', [
			'id' => $change_id,
		], $value) );
	}

	/**
	 * Prints out arbitrary html, useful for one-time-use scripts
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_html_field( $value = '', $name = '', $args = [] ) {
		return $args['html'];
	}

	/**
	 * Adds a save or update button that ties in to the WordPress save button
	 *
	 * @param string $value
	 * @param string $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_save_button_field( $value = '', $name = '', $args = [] ) {
		return $this->display_field_wrap( 'save-button', Markup::tag( 'button', [
			'type'  => 'submit',
			'name'  => 'update',
			'class' => [ 'button', 'button-primary' ]
		], 'Update' ) );
	}

	/**
	 * Alias for save_button
	 *
	 * @param $value
	 * @param $name
	 * @param $args
	 *
	 * @return string
	 */
	protected function display_update_button_field( $value, $name, $args ) {
		return $this->display_save_button_field( $value, $name, $args );
	}

	/**
	 * Displays a media upload button and url input box
	 * @param $value
	 * @param $name
	 * @param array $args
	 * @return string
	 */
	protected function display_media_field($value, $name, $args = []) {

		$label = !empty($value) ? __('Change', $this->get('textdomain')) : __('Select', $this->get('textdomain'));
		if(isset($args['button_label']) && !empty($args['button_label'])) {
			$label = $args['button_label'];
		}

		$html = Markup::tag('input', [
			'type' => 'text',
			'class' => 'code',
			'id'   => call_user_func_array( [ $this, 'get_html_id' ], (array) $name ),
			'name' => call_user_func_array( [ $this, 'get_input_name' ], (array) $name ),
		]);

		$html .= Markup::tag( 'button', [
			'class'   => 'button',
			'onclick' => 'WMTheme.upload.media(this, event); return false;',
			'data-frame-label' => $label,
		], $label);

		return $this->display_field($args, $name, Markup::tag('div', [
			'class' => 'mazloy-media-upload'
		], $html));
	}

	/**
	 * @param $value
	 * @param $name
	 * @param array $args
	 *
	 * @return string
	 */
	protected function display_color_field( $value, $name, $args = [] ) {
		$args['type'] = 'color-picker';
		return $this->display_field( $args, $name, Markup::tag( 'input', [
			'type'  => 'text',
			'id'    => call_user_func_array( [ $this, 'get_html_id' ], (array) $name ),
			'name'  => call_user_func_array( [ $this, 'get_input_name' ], (array) $name ),
			'value' => $value
		] ) );
	}

	/**
	 * Displays an upload image button, image preview and url box
	 * @param $value
	 * @param $name
	 * @param array $args
	 * @return string
	 */
	protected function display_image_field( $value, $name, $args = [] ) {
		$value = wp_parse_args($value, [
			'url' => '',
			'id' => ''
		]);

		$has_image = !empty($value['url']);

		$previewWrapperClass = ['mazloy-image-preview-wrap'];

		if($has_image) {
			$previewWrapperClass[] = 'has-image';
		}

		$html = Markup::tag( 'div', [ 'class' => $previewWrapperClass ], Markup::tag( 'div', [
			'class'   => 'mazloy-image-preview',
			'onclick' => 'WMTheme.upload.image(this, event);return false;',
			'style'   => [
				'display' => $has_image ? 'block' : 'none'
			],
			'data-label-change' => __('Change Image', $this->get('textdomain'))
		], $has_image ? Markup::tag( 'img', [ 'src' => $value['url'], 'alt' => '' ] ) : '' ) );

		$html .= Markup::tag( 'input', [
			'type'  => 'text',
			'disabled' => 'disabled',
			'value' => $value['url'],
			'data-image-upload-url' => '',
			'style'   => [
				'display' => $has_image ? 'block' : 'none'
			]
		] );

		$html .= Markup::tag('p', [], Markup::tag( 'button', [
			'class'   => 'button',
			'onclick' => 'WMTheme.upload.image(this, event); return false;',
			'data-label-change' => __('Change Image', $this->get('textdomain')),
		], Markup::icon('upload') . __($has_image ? 'Change Image' : 'Upload' , $this->get('textdomain'))));

		$html .= Markup::tag( 'input', [
			'type'  => 'hidden',
			'id'   => call_user_func_array( [ $this, 'get_html_id' ], array_merge((array) $name, ['url'] )),
			'name' => call_user_func_array( [ $this, 'get_input_name' ], array_merge((array) $name, ['url'])),
			'value' => $value['url'],
			'data-image-upload-url' => ''
		] );

		$html .= Markup::tag( 'input', [
			'type'  => 'hidden',
			'id'   => call_user_func_array( [ $this, 'get_html_id' ], array_merge((array) $name, ['id'] )),
			'name' => call_user_func_array( [ $this, 'get_input_name' ], array_merge((array) $name, ['id'])),
			'value' => $value['id'],
			'data-image-upload-id' => ''
		] );


		return $this->display_field($args, $name, Markup::tag('div', ['class' => 'mazloy-image-upload'], $html));
	}

	//////////////////////////////////////////////////
	//  Private helpers
	//////////////////////////////////////////////////

	/**
	 * Returns the list of already registered metaboxes saved inside the theme container
	 * @return array|mixed|null
	 */
	private function registeredMetaboxes() {
		return $this->get( 'quick_metaboxes' ) ? $this->get( 'quick_metaboxes' ) : [];
	}

	/**
	 * Parses options from a field argument set and returns all available options
	 * even if they don't match the field. This prevents default functions like
	 * display_field from error-ing out
	 *
	 * @param $field
	 *
	 * @return array
	 */
	private function parseField( $field = [] ) {
		return wp_parse_args( $field, [
			'type'        => 'text',
			'label'       => 'Label One',
			'sub_label'   => '',
			'default'     => '',
			'description' => '',
			'options'     => [],
			'multiple'    => false,
			'required'    => false
		] );
	}

	/**
	 * Checks for sequential ids and changes the keys to use the value. this is meant
	 * to be used with select, multi-checkbox and multi-radio fields.
	 *
	 * @param array $options
	 *
	 * @return array
	 */
	private function associativeOptions( $options = [] ) {
		$output      = [];
		$associative = array_keys( $options ) !== range( 0, count( $options ) - 1 );
		foreach ( $options as $key => $value ) {
			$key            = $associative ? $key : $value;
			$output[ $key ] = $value;
		}

		return $output;
	}
}