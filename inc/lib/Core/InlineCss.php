<?php

namespace Mazloy\Core;

use Mazloy\Core\Abstracts\RunableAbstract;
use Mazloy\Core\InlineCss\RuleCollection;

class InlineCss extends RunableAbstract
{
	/**
	 * WP Head output callback
	 */
	public function output() {
		$minify = $this->get('env.production');

		$rules = [

		];

		echo Markup::comment("BOF - mazloy auto-generated styles");
		echo Markup::tag( 'style', [
			'type'  => 'text/css',
			'id'    => 'theme-generated-css',
			'media' => 'screen'
		], ($minify ? '' : "\n" ) . ( new RuleCollection( $rules, $minify ) )->getOutput() );
		echo Markup::comment("EOF - mazloy auto-generated styles") . "\n";
	}

	/**
	 * @inheritdoc
	 */
	public function run() {
		$this->loader()->addAction( 'wp_head', [ $this, 'output' ], 99 );
	}
}