<?php

namespace Mazloy\Core\Abstracts;

use Mazloy\Core\Container;
use Mazloy\Core\Loader;

abstract class RunableAbstract
{
	/**
	 * @var Container
	 */
	protected $container;

	/**
	 * RunableAbstract constructor.
	 *
	 * @param Container $container
	 */
	public function __construct(Container &$container)
	{
		$this->container = $container;
	}

	/**
	 * @return Loader
	 */
	public function loader()
	{
		return $this->container->get('loader');
	}

	/**
	 * @param string $name
	 * @return mixed|null
	 */
	public function get($name = '')
	{
		return $this->container->get($name);
	}

	/**
	 * Includes files passed using require once
	 * @param array $files
	 * @return void
	 */
	protected function includeArbitraryFiles( $files = []) {
		foreach ((array) $files as $file)
			if(file_exists($file))
				require_once $file;
	}

	/**
	 * @return void
	 */
	abstract public function run();
}