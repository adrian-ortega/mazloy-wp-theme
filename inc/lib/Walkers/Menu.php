<?php

namespace Mazloy\Walkers;

class Menu extends \Walker_Nav_Menu
{
	/**
	 * @inheritdoc
	 */
	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
		if( $element->current || $element->current_item_ancestor )
			$element->classes[] = 'menu-item--active';

		parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}

	/**
	 * @inheritdoc
	 */
	function start_lvl(&$output, $depth = 0, $args = array())
	{
		$t = "\t";
		$n = "\n";

		if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
			$t = '';
			$n = '';
		}

		$indent = str_repeat( $t, $depth );

		$classes = join(' ' , [
			'sub-menu',
			'sub-menu-level-' . $depth
		]);

		$output .= "{$n}{$indent}<span class='handle'><i class='mdi mdi-chevron-down'></i></span>\n";
		$output .= "{$n}{$indent}<ul class=\"{$classes}\">\n";
	}
}