<?php

namespace Mazloy\Template;

use Mazloy\Core\Abstracts\RunableAbstract;

class BodyClass extends RunableAbstract
{
	/**
	 * Adds classes to the body tag based on features within the theme options
	 * @param array $classes
	 *
	 * @return array
	 */
	public function getClasses( $classes = [] ) {

		if(mazloy_enabled('loader')) {
			$classes[] = 'mazloy-loader-enabled';
			$classes[] = 'mazloy-loader-show';
		}

		return $classes;
	}

	public function run() {
		$this->loader()->addFilter('body_class', [$this, 'getClasses']);
	}
}