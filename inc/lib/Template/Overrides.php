<?php

namespace Mazloy\Template;

use Mazloy\Core\Abstracts\RunableAbstract;

class Overrides extends RunableAbstract
{
	public function blog($wrappers, $template) {
		if(in_array($template, ['home', 'archive', 'single', 'single-post'])) {
			array_unshift( $wrappers, 'layouts/base-blog.php' );
		}

		return $wrappers;
	}

	public function run() {
		$this->loader()->addFilter('wm_template_wrap_base', [$this, 'blog'], 10, 2);
	}
}