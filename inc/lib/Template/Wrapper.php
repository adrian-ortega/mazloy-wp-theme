<?php

namespace Mazloy\Template;

class Wrapper
{
    public $slug;
    public $template;
    public $wrapper;
	public $template_slug;

	public function __construct($template, $layout = 'layouts/base.php')
    {
        $this->slug = sanitize_title(basename($layout, '.php'));
        $this->wrapper = [$layout];
        $this->template = $template;
        $this->template_slug = basename($template, '.php');
        $str = substr($layout, 0, -4);
	    array_unshift($this->wrapper, sprintf($str . '-%s.php', $this->template_slug));

    }

	public function getWrappers() {
		return apply_filters('wm_template_wrap_' . $this->slug, $this->wrapper, $this->template_slug) ?: $this->wrapper;
    }

	public function getMainFiles() {
		return apply_filters('wm_template_unwrap_' . $this->slug, $this->template) ?: $this->template;
    }

    /**
     * Get the wrapper template file
     * @return string
     */
    public function wrap()
    {
    	return locate_template($this->getWrappers());
    }

    /**
     * Get the slug
     * @return string
     */
    public function slug()
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function unwrap()
    {
	    $template = $this->getMainFiles();
        return locate_template($template) ?: $template;
    }
}