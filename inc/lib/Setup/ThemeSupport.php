<?php

namespace Mazloy\Setup;

use Mazloy\Core\Abstracts\RunableAbstract;

class ThemeSupport extends RunableAbstract
{
    public function supports()
    {
        // Adds the ability for plugins and themes to handle the themes title tag
        // instead of using wp_title
        add_theme_support('title-tag');

        // Adds featured images to posts
        add_theme_support('post-thumbnails');

        // Adds HTML5 markup for the theme
        add_theme_support('html5', [
            'caption',
            'comment-form',
            'comment-list',
            'gallery',
            'search-form'
        ]);

        // Add theme support for selective refresh for widgets.
        add_theme_support( 'customize-selective-refresh-widgets' );

        // Custom Image Sizes
        add_image_size('mazloy-preview-admin', 100, 100, true);
        add_image_size('mazloy-preview', 870, 400);
        add_image_size('mazloy-single', 870, 653);
        add_image_size('mazloy-related-post', 350, 263, true);

        // Adds the frontend stylesheet to the editor
        add_editor_style($this->get('theme_css') . 'app.css');

        // Registers our menu's
        register_nav_menus([
            'utility' => __('Utility Menu', $this->get('textdomain')),
            'copyright' => __('Copyright Footer Menu', $this->get('textdomain')),
            'main' => __('Main Menu', $this->get('textdomain')),
        ]);
    }

    /**
     *  Registers default widget areas
     */
    public function widgetAreas()
    {
        foreach(['right', 'left'] as $side) {
            register_sidebar( [
		        'id'            => "sidebar-{$side}",
		        'name'          => sprintf(__( 'Sidebar %s', $this->get( 'textdomain' ) ), ucwords($side)),
		        'description'   => '',
		        'before_widget' => "<div class=\"widget sidebar-widget sidebar-widget--{$side} %2\$s\">",
		        'after_widget'  => '</div>',
		        'before_title'  => '<h4 class="widget__title">',
		        'after_title'   => '</h4>',
	        ] );
        }

        for($i = 1; $i <= 4; $i++)
            register_sidebar([
                'id'            => "footer-{$i}",
                'name'          => sprintf(__('Footer %d', $this->get('textdomain')), $i),
                'description'	=> '',
                'before_widget' => '<div class="widget footer-widget %2$s">',
                'after_widget'  => '</div>',
                'before_title'	=> '<h4 class="widget__title">',
                'after_title'	=> '</h4>',
            ]);
    }

    /**
     * Add support for buttons in the top-bar menu:
     * 1) In WordPress admin, go to Appearance -> Menus.
     * 2) Click 'Screen Options' from the top panel and enable 'CSS CLasses' and 'Link Relationship (XFN)'
     * 3) On your menu item, type 'has-form' in the CSS-classes field. Type 'button' in the XFN field
     * 4) Save Menu. Your menu item will now appear as a button in your top-menu
     *
     * @param string $ulClass
     * @return string
     */
    public function menuButtons($ulClass)
    {
        return preg_replace(
            ['/<a rel="button"/', '/<a title=".*?" rel="button"/'],
            ['<a rel="button" class="button"', '<a rel="button" class="button"'],
            $ulClass,
            1
        );
    }

    /**
     * Prints the logo on the login screen
     * @return bool|void
     */
    public function loginLogo()
    {
        $file = $ext = $name = false;

        foreach(['png', 'jpg', 'jpeg', 'gif'] as $_ext) {
            foreach([ 'logo-login', 'login-logo', 'logo' ] as $_file) {
                if (file_exists($this->get('theme.path') . "/assets/images/{$_file}.{$_ext}")) {
                    $name = $_file;
                    $file = $this->get('assets.images') . "{$_file}.{$_ext}";
                    $ext = $_ext;
                    break 2;
                }
            }
        }

        if(!$file) return;
        $imageSize = getimagesize($file);
        $width  = $imageSize[0];
        $height = $imageSize[1];
        ?>
        <!-- custom <?php echo $this->get('theme.short_name'); ?> login logo -->
        <style>
            body.login {
                background: #222222;
            }

            body.login #backtoblog a,
            body.login #nav a {
                color: #ffffff;
            }

            body.login #backtoblog a:hover,
            body.login #nav a:hover {
                color: #cccccc;
            }

            #login {
                width: 400px !important;
                padding-top: 6% !important;
            }

            .login h1 a {
                display: block;
                background-image: url("<?php mazloy_image("{$name}.{$ext}") ?>") !important;
                background-size: <?php echo $width ?>px !important;
                width: <?php echo $width ?>px !important;
                height: <?php echo $height ?>px !important;

                margin-bottom: 60px !important;
            }
        </style>
        <!-- /custom <?php echo $this->get('theme.short_name'); ?> login logo -->
        <?php
    }

    public function rssImage()
    {
        global $post;
        if(has_post_thumbnail($post->ID)) {
            $thumbnail_id = get_post_thumbnail_id($post->ID);
            $thumbnail = wp_get_attachment_image_src($thumbnail_id, 'thumbnail');

            echo "<image>{$thumbnail[0]}</image>" ;
        }
    }

	public function menuLinkAttributes( $atts, $item, $args ) {
        $atts['itemprop'] = 'url';
        $atts['role'] = 'menuitem';

        return $atts;
    }

	public function readMoreEllipsis() {
        return '&hellip;';
    }

    public function run()
    {
        $this->loader()->addAction('after_setup_theme', [$this, 'supports']);
        $this->loader()->addAction('widgets_init', [$this, 'widgetAreas']);
        $this->loader()->addFilter('wp_nav_menu', [$this, 'menuButtons']);
        $this->loader()->addAction('login_enqueue_scripts', [$this, 'loginLogo']);
        $this->loader()->addAction('rss2_item', [$this, 'rssImage']);
        $this->loader()->addFilter('widget_text', 'do_shortcode');
        $this->loader()->addFilter('nav_menu_link_attributes', [$this, 'menuLinkAttributes'], 10, 3);
        $this->loader()->addFilter('excerpt_more', [$this, 'readMoreEllipsis']);
    }
}