<?php

namespace Mazloy\Setup;

use Mazloy\Core\JsonConfig as JsonConfigBase;

class JsonConfig extends JsonConfigBase
{
    protected function errorTitle()
    {
        return 'Missing main JSON config file:';
    }

    protected function errorMessage()
    {
        $msg  = '<h3 style="color:#666;font-size:16px;">';
        $msg .= 'This theme requires the <strong><code>config.json</code></strong> file to run.</h3>';
        $msg .= '<p>It is used for configuration within main PHP files, JavaScript files as well as a few SCSS/CSS files. ';
        $msg .= 'Please run <strong><code>gulp</code></strong> using your terminal or command prompt to create it. ';
        $msg .= 'All tasks run using gulp will create it automatically. You can customize its output using ';
        $msg .= '<code>userconfig.json</code></p>';
        $msg .= '<p><a href="http://wp.aortegadesign.com/getting-started/node-modules-bower-and-gulp/" ';
        $msg .= 'class="button secondary-button" title="Node Modules, Bower and Gulp" ';
        $msg .= 'target="_blank">You can read more about this here &raquo;</a></p>';

        return $msg;
    }
}