<?php

namespace Mazloy\Setup;

use Mazloy\Core\Abstracts\RunableAbstract;
use Mazloy\Template\Template;
use Mazloy\Template\Wrapper;

class TemplateSupport extends RunableAbstract
{
	/**
	 * @var Template
	 */
	protected $_template;

	/**
     * Returns the correct filename redirected to `/templates/`
     * @param string $main
     * @return string
     */
    public function templateWrapper($main)
    {
        if (!is_string($main) && !(is_object($main) && method_exists($main, '__toString')))
            return $main;
	    $this->_template = new Template( new Wrapper($main) );

        return $this->_template->layout();
    }

    /**
     * Returns the main theme path
     * @return string
     */
    public function templateDirectory()
    {
        return $this->get('theme.path');
    }

    /**
     * Returns the main theme directory name
     * @param $stylesheet
     * @return string
     */
    public function template($stylesheet)
    {
        return dirname($stylesheet);
    }

	/**
	 * This will loop through all layouts and only add the first one that exists to the body class
	 * @param array $classes
	 *
	 * @return array
	 */
	public function bodyClass( $classes = []) {

    	foreach($this->_template->wrapper->getWrappers() as $wrapper) {
			if(file_exists(mazloy('theme.path') . "templates/{$wrapper}")) {
				$classes[] = 'mazloy-layout-' . basename($wrapper, '.php');
				break;
			}
		}

		return $classes;
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->loader()->addFilter('template', [$this, 'template'], PHP_INT_MAX);
        $this->loader()->addFilter('template_directory', [$this, 'templateDirectory'], PHP_INT_MAX, 3);
        $this->loader()->addFilter('template_include', [$this, 'templateWrapper'], PHP_INT_MAX);
        $this->loader()->addFilter('body_class', [$this, 'bodyClass']);
    }
}