<?php

namespace Mazloy\PostTypes;

class HomePageSlider extends Slider
{
	public $post_type = 'mazloy-home-slide';
	protected $icon = 'dashicons-slides';
	protected $menu_label = 'mazloy Home Slider';
	protected $has_taxonomy = false;
}