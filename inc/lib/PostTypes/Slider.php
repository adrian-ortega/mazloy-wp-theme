<?php

namespace Mazloy\PostTypes;

use Mazloy\Core\CustomPostType;

class Slider extends CustomPostType
{
	public $post_type = 'mazloy-slide';
	protected $icon = 'dashicons-images-alt2';
	protected $menu_label = 'mazloy Slider';
	protected $has_taxonomy = true;

	protected function setup() {
		$text_domain = $this->get('textdomain');
		$this->post_args = [
			'publicly_queryable' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'menu_position' => 22,
			'menu_icon' => $this->icon,
			'supports' => ['title'],
			'capability_type' => 'post'
		];

		$this->labels('Slide', 'Slides', [
			'name' => _x( $this->menu_label, 'post type general name', $text_domain ),
			'menu_name' => _x( $this->menu_label, 'admin menu', $text_domain )
		]);

		if($this->has_taxonomy) {
			$this->taxonomy('slide-location', __('Slide Location', $text_domain), __('Slide Locations', $text_domain), [

			]);
		}

		$this->metabox('slide-options-' . $this->post_type, [
			'name' => __('Slide Options'),
			'metakey' => 'wm_slide',
			'use_single_keys' => false,
			'fields' => [
				'slide_note' => [
					'label' => '',
					'type' => 'note',
					'description' => __('Please fill out & configure the fileds below to create your slide. The only mandatory field is the "Slide Image".', $text_domain)
				],
				'slide_type' => [
					'type' => 'button_option',
					'label' => __('Background Type', $text_domain),
					'sub_label' => __('Select the background type you\'d like to use for your slide'),
					'default' => 'image',
					'options' => [
						'image' => __('Background Image', $text_domain),
						'video' => __('Background Video', $text_domain),
					]
				],
				'slide_image' => [
					'type' => 'image',
					'label' => __('Slide Image', $text_domain),
					'sub_label' => __('Click the "Upload" button to begin uploading your image, followed by "Select Image" once you\'ve made your selection', $text_domain),
					'required' => ['slide_type', '=', 'image']
				],
				'slide_video_recommendations' => [
					'type' => 'note',
					'label' => __('Video Recommendations'),
					'description' => '<p>When using a video, try to use videos that won\'t annoy your users e.g. a video with no sound.</p><p><strong>Please note that you must include <code>webm</code> and the <code>mp4</code> format to render your video with cross browser compatibility. <code>OGV</code> is optional. Video must be in 16:9 aspect ratio</strong></p>',
					'required' => ['slide_type', '=', 'video']
				],
				'slide_video_webm' => [
					'type' => 'media',
					'label' => __('Video WebM Upload', $text_domain),
					'sub_label' => __('Browse for your WebM video file here', $text_domain),
					'button_label' => 'Select WEBM',
					'description' => __('You can select/upload a video or type in a URL', $text_domain),
					'required' => ['slide_type', '=', 'video']
				],
				'slide_video_mp4' => [
					'type' => 'media',
					'label' => __('Video MP4 Upload', $text_domain),
					'sub_label' => __('Browse for your MP4 video file here', $text_domain),
					'button_label' => 'Select MP4',
					'description' => __('You can select/upload a video or type in a URL', $text_domain),
					'required' => ['slide_type', '=', 'video']
				],
				'slide_video_ogv' => [
					'type' => 'media',
					'label' => __('Video OGV Upload', $text_domain),
					'sub_label' => __('Browse for your OGV video file here', $text_domain),
					'button_label' => 'Select OGV',
					'description' => __('You can select/upload a video or type in a URL', $text_domain),
					'required' => ['slide_type', '=', 'video']
				],
				'slide_video_preview' => [
					'type' => 'image',
					'label' => __('Video Preview Image', $text_domain),
					'sub_label' => __('This is the image that will be seen in place of your video on mobile devises & older browsers before your video is played', $text_domain),
					'required' => ['slide_type', '=', 'video']
				],
				'slide_background_align' => [
					'type' => 'button_option',
					'label' => __('Background Alignment', $text_domain),
					'sub_label' => __('Please choose how you would like your slides background to be aligned'),
					'default' => 'center',
					'options' => [
						'top' => 'Top',
						'center' => 'Center',
						'bottom' => 'Bottom'
					]
				],
				'slide_font_color' => [
					'type' => 'button_option',
					'label' => __('Slide Font Color', $text_domain),
					'sub_label' => __('This gives you an easy way to make sure your text is visible regardless of the background', $text_domain),
					'default' => 'light',
					'options' => [
						'light' => __('Light', $text_domain),
						'dark' => __('Dark', $text_domain)
					]
				],
				'slide_heading' => [
					'type' => 'text',
					'label' => __('Heading', $text_domain),
					'sub_label' => __('Slide heading', $text_domain),
				],
				'slide_caption' => [
					'type' => 'textarea',
					'label' => __('Caption', $text_domain),
					'description' => 'HTML is allowed'
				],
				'slide_caption_background' => [
					'type' => 'switch',
					'label' => __('Caption Background', $text_domain),
					'sub_label' => __('If you would like to add a semi transparent background to your caption, activate this option', $text_domain),
					'default' => 0
				],
				'slide_down_arrow' => [
					'type' => 'switch',
					'label' => __('Down Arrow', $text_domain),
					'sub_label' => __('Insert a down arrow that leads to content', $text_domain),
					'default' => 0
				],
				'slide_link_type' => [
					'type' => 'button_option',
					'label' => __('Link Type', $text_domain),
					'sub_label' => __('Please select how you would like to link your slide', $text_domain),
					'default' => 'buttons',
					'options' => [
						'buttons' => __('Buttons', $text_domain),
						'link' => __('Full Slide Link', $text_domain)
					]
				],
				'slide_button_1' => [
					'type' => 'button',
					'label' => __('Button #1'),
					'sub_label' => __('Configure your button here', $text_domain),
					'required' => ['slide_link_type', '=', 'buttons']
				],
				'slide_button_2' => [
					'type' => 'button',
					'label' => __('Button #1'),
					'sub_label' => __('Configure your button here'),
					'required' => ['slide_link_type', '=', 'buttons']
				],
				'slide_button_full_url' => [
					'type' => 'url',
					'label' => __('Slide Link URL', $text_domain),
					'sub_label' => __('The url the entire slide will link to'),
					'placeholder' => 'http://Mazloy.com',
					'required' => ['slide_link_type', '=', 'link']
				],
				'slide_content_align' => [
					'type' => 'element_position',
					'label' => __('Slide Content Alignment', $text_domain),
					'sub_label' => __('Configure the position for your slides content', $text_domain),
				],
				'css_class' => [
					'type' => 'text',
					'label' => __('Extra CSS Classes', $text_domain),
					'sub_label' => __('Use this to add one or more css classes.', $text_domain),
					'placeholder' => 'example-class some-other-class',
					'description' => 'Please use space separated class names to add multiple classes',
				],
				'save_button' => [
					'type' => 'update_button'
				],
				'console_logger' => [
					'type' => 'html',
					'html' => '<div class="field field-console"><a href="#" onclick="WMTheme.metaboxLogger(this, \'wm_slide\', event)">Console Log Data</a></div>'
				],
//				'arbitrary_html' => [
//					'type' => 'html',
//					'html' => '<script>document.getElementById("post-body-content").style.display = "none";</script>'
//				]
			]
		]);
	}
}