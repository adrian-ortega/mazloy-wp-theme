<?php

namespace Mazloy\Assets;

use Mazloy\Core\DotNotation;
use Mazloy\Core\Markup;
use Mazloy\Core\QuickMetabox;
use Mazloy\Core\WmObject;

class Slide extends WmObject
{
	/**
	 * @var QuickMetabox
	 */
	protected $metabox;

	/**
	 * @var array
	 */
	public $meta;

	public function __construct( $post ) {
		if(is_int($post))
			$post = get_post($post);

		$this->metabox = mazloy('quick_metabox_slide-options-' . $post->post_type);
		$this->meta = $this->metabox->get_meta();

		$data = [];
		$data['type'] = $this->meta['slide_type'];
		$data['background_align'] = $this->meta['slide_background_align'];

		$data['image'] = wp_parse_args((array) $this->meta['slide_image'], [
			'url' => '',
			'id' => -1
		]);

		$data['video'] = [
			'webm'    => esc_url( $this->meta['slide_video_wbm'] ),
			'mp4'     => esc_url( $this->meta['slide_video_mp4'] ),
			'ogv'     => esc_url( $this->meta['slide_video_ogv'] ),
			'preview' => wp_parse_args((array) $this->meta['slide_video_preview'], [
				'url' => '',
				'id' => -1
			]),
		];

		$data['font_color'] = $this->meta['slide_font_color'];
		$data['heading'] = $this->meta['slide_heading'];
		$data['caption'] = !empty($this->meta['slide_caption']) ? apply_filters('the_content', $this->meta['slide_caption']) : '';

		// Switches
		$data['caption_background'] = $this->meta['slide_caption_background'] == '1';
		$data['link_arrow'] = $this->meta['slide_link_arrow'] == '1';
		$data['full_link'] = $this->meta['slide_link_type'] == 'link';

		foreach(['1', '2'] as $b) {
			$saved = (array) $this->meta['slide_button_1'];
			$b = "button{$b}";
			$data[$b] = [];
			foreach(['text', 'url', 'style', 'color'] as $k) {
				$data[$b][$k] = isset( $saved[$k] ) && ! empty( $saved[$k] ) ? $saved[$k] : null;
			}
		}

		$data['slide_link'] = isset($this->meta['slide_button_full_url']) && !empty($this->meta['slide_button_full_url']) ? esc_url($this->meta['slide_button_full_url']) : null;
		$data['content_align'] = wp_parse_args($this->meta['slide_content_align'], [
			'horizontal' => 'center',
			'vertical' => 'center',
		]);

		$data['css_class'] = array_filter( explode(' ', $this->meta['css_class']) );

		$this->data = $data;
	}

	/**
	 * Returns HTML attributes for a slide wrapper
	 * @return string
	 */
	public function slideAttributes() {
		$attributes = [
			'class' => ['slide__image'],
			'style' => []
		];

		if($this->get('type') == 'image') {
			if($this->has('image')) {
				$attributes['style']['background-image'] = "url({$this->get('image.url')})";
			}
		}

		if($this->get('background_align')) {
			$attributes['class'][] = 'slide__image--bg-' . $this->get('background_align');
		}

		return Markup::attributes($attributes);
	}

	/**
	 * @return bool
	 */
	public function is_image() {
		return $this->get('type') == 'image';
	}

	/**
	 * Returns output from a video shortcode
	 * @return string
	 */
	public function video_shortcode() {
		$args = [];
		if($this->has('video.mp4')) { $args[] = "mp4=\"{$this->get('video.mp4')}\""; }
		if($this->has('video.ogv')) { $args[] = "ogv=\"{$this->get('video.ogv')}\""; }
		if($this->has('video.preview')) { $args[] = "poster=\"{$this->get('video.preview')}\""; };
		$args = implode(' ', $args);
		return do_shortcode("[video {$args}]");
	}

	/**
	 * Checks to see if heading and caption exist
	 * @return bool
	 */
	public function has_content() {
		return $this->has('heading') || $this->has('caption');
	}

	/**
	 * Returns html markup for a button
	 * @param int $which
	 *
	 * @return null|string
	 */
	public function button( $which = 1 ) {
		if($this->get('full_link')) {
			$prefix = "button{$which}";
			return Markup::tag('a', [
				'href' => $this->get("{$prefix}.url"),
				'title' => $this->get("{$prefix}.text"),
				'class' => [
					"button",
					"button--{$this->get("{$prefix}.style")}",
					"button--{$this->get("{$prefix}.color")}",
				]
			], $this->get("{$prefix}.text"));
		}
		return null;
	}

	public function rowAttributes() {
		$row_attributes = [
			'class' => ['row']
		];

		if($this->has('content_align.horizontal')) {
			switch($this->get('content_align.horizontal')) {
				case 'left': $row_attributes['class'][] = 'start-xs'; break;
				case 'center': $row_attributes['class'][] = 'center-xs'; break;
				case 'right': $row_attributes['class'][] = 'end-xs'; break;
			}
		}

		if($this->has('content_align.vertical')) {
			switch($this->get('content_align.vertical')) {
				case 'top': $row_attributes['class'][] = 'top-xs'; break;
				case 'center': $row_attributes['class'][] = 'middle-xs'; break;
				case 'bottom': $row_attributes['class'][] = 'bottom-xs'; break;
			}
		}

		echo Markup::attributes($row_attributes);
	}

}