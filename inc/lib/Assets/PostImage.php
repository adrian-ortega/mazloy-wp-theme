<?php
namespace Mazloy\Assets;

use WP_Post;

class PostImage
{
	/**
	 * @var int
	 */
	public $ID;

	/**
	 * @var string
	 */
	public $url;

	/**
	 * @var int
	 */
	public $width;

	/**
	 * @var int
	 */
	public $height;

	/**
	 * @var string
	 */
	protected $size = 'thumbnail';

	/**
	 * @var \WP_Post
	 */
	protected $post;

	/**
	 * @var array
	 */
	protected $meta;

	public function __construct(WP_Post &$post)
	{
		$this->post = $post;
		$this->ID = (int) get_post_thumbnail_id($post->ID);

		return $this;
	}

	/**
	 * @param string|int $image_id
	 * @return $this
	 */
	public function setImageId($image_id)
	{
		$this->ID = (int) $image_id;
		return $this;
	}

	/**
	 * Sets the size to be retrieved
	 * @param string $size
	 * @return $this
	 */
	public function setSize($size)
	{
		$this->size = $size;
		return $this;
	}

	/**
	 * Retrieves all image information
	 * @return $this
	 */
	public function build()
	{
		if ($this->ID) {

			$this->url = wp_get_attachment_image_url($this->ID, $this->size);

			if ($this->meta = wp_get_attachment_metadata($this->ID)) {
				if (isset($this->meta['sizes'][$this->size])) {
					$this->width = $this->meta['sizes'][$this->size]['width'];
					$this->height = $this->meta['sizes'][$this->size]['height'];
				} else {
					$this->width = $this->meta['width'];
					$this->height = $this->meta['height'];
				}
			}
		}

		return $this;
	}

	public function aspect_ratio()
	{
		return number_format((absint($this->height) / absint($this->width) * 100), 6);
	}

	/**
	 * Retrieves size image by size
	 * @param string $size
	 * @return null
	 */
	public function getSize($size)
	{
		if(empty($this->url))
			$this->build();

		return isset($this->meta['sizes'][$size]) ? $this->meta['sizes'][$size] : null;
	}
}