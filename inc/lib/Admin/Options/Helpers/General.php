<?php

namespace Mazloy\Admin\Options\Helpers;

class General
{
    public static function numbers($start = 1, $end = 10){
        $choices = [];
        for($i = $start; $i <= $end; $i++) {
            $choices[$i] = $i;
        }
        return $choices;
    }

	public static function fontFamily($id, $label, $desc) {
		return [
			'id'       => $id,
			'type'     => 'typography',
			'title'    => $label,
			'subtitle' => $desc,
			'google'   => true,
			'default'  => []
		];
    }

    public static function fontWeights()
    {
        return [
            'light' => 'Light',
            'normal' => 'Normal',
            'bold' => 'Bold',
            '100' => '100',
            '200' => '200',
            '300' => '300',
            '400' => '400',
            '500' => '500',
            '600' => '600',
            '700' => '700',
            '800' => '800',
        ];
    }

    public static function loremLong()
    {
        return 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ' .
            'ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ' .
            'ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ' .
            'reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur ' .
            'sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id ' .
            'est laborum.';
    }
}