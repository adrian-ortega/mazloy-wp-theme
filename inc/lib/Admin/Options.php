<?php

namespace Mazloy\Admin;

use Mazloy\Admin\Options\Helpers\General;
use Mazloy\Core\Container;
use Mazloy\Core\Abstracts\RunableAbstract;
use Redux;

class Options extends RunableAbstract
{
	/**
	 * Options page slug for the admin
	 * @var string
	 */
	public $page_slug = 'mazloy-options';

	/**
	 * Key name used to store options in the database
	 * @var string
	 */
	public $options_name = 'website_muscle_options';

	/**
	 * All options registered with Redux
	 * @var array
	 */
	public $options = [];

	/**
	 * Help tabs registered with Redux
	 * @var array
	 */
	public $helpTabs = [];

	/**
	 * Sections registered with Redux
	 * @var array
	 */
	public $sections = [];

	/**
	 * @var Container
	 */
	protected $container;

	public function compiler($options, $css, $changed_values) {
		// @TODO Adrian Ortega - Figure out how to add a CSS compiler with Gulp
	}

	/**
	 * We have to add a quick fix to the menu icon since it's a big svg
	 */
	public function adminHead() {
		echo '<style>';
		echo '#toplevel_page_wm-options div.wp-menu-image > img {';
		echo 'width:20px; height:16px;';
		echo '}';
		echo '</style>';
	}

	/**
	 * Returns the proper admin menu item icon url
	 * @return null|string
	 */
	private function getMenuIcon() {
		$url = null;
		if(floatval(get_bloginfo('version')) >= "3.8") {
			// @TODO Adrian Ortega - Check to see if we need to change icon color when on a light theme
			// $current_color = get_user_option( 'admin_color' );
			$path = $this->get('assets.images') . 'admin';
			$url = "{$path}/icons/mazloy-logo-menu.png";
		}

		return $url;
	}
	/**
	 * Save all options
	 * @return void
	 */
	private function initReduxOptions() {
		$this->options = [
			'opt_name'           => $this->options_name,
			'disable_tracking'   => true,
			'dev_mode'           => false,
			'use_cdn'            => true,
			'save_defaults'      => true,

			// Display
			'display_name'         => $this->get( 'theme.name' ),
			'display_version'      => $this->get('debug') ? $this->get( 'theme.version' ) : null,

			// Menu options
			'page_slug'          => $this->page_slug,
			'page_title'         => __( 'Mazloy Options', $this->get( 'textdomain' ) ),
			'page_priority'      => 54,
			'page_parent'        => 'themes.php',
			'page_permissions'   => 'manage_options',

			// Dev options
			'update_notice'      => true,
			'admin_bar'          => true,
			'menu_type'          => 'menu',
			'menu_title'         => __( 'Mazloy', $this->get( 'textdomain' ) ),
			'menu_icon'          => $this->getMenuIcon(),
			'allow_sub_menu'     => true,
			'customizer'         => true,
			'default_mark'       => '',

			// Hints
			'hints'              => [
				'icon_position' => 'right',
				'icon_color'    => 'lightgray',
				'icon_size'     => 'normal',
				'tip_style'     => [
					'color' => 'light',
				],
				'tip_position'  => [
					'my' => 'top left',
					'at' => 'bottom right',
				],
				'tip_effect'    => [
					'show' => [
						'duration' => '500',
						'event'    => 'mouseover',
					],
					'hide' => [
						'duration' => '500',
						'event'    => 'mouseleave unfocus',
					],
				],
			],

			'output'             => true,
			'output_tag'         => true,
			'settings_api'       => true,
			'cdn_check_time'     => HOUR_IN_SECONDS,
			'compiler'           => true,
			'show_import_export' => true,
			'transient_time'     => HOUR_IN_SECONDS ,
			'network_sites'      => false,

			// Social Networks
			'share_icons'        => [
				[
					'url'   => 'https://www.facebook.com/Mazloy',
					'title' => 'Like us on Facebook',
					'icon'  => 'el el-facebook',
				], [
					'url'   => 'https://twitter.com/Mazloy',
					'title' => 'Follow us on Twitter',
					'icon'  => 'el el-twitter'
				], [
					'url'   => 'https://www.linkedin.com/company/website-muscle',
					'title' => 'Find us on LinkedIn',
					'icon'  => 'el el-linkedin'
				], [
					'url'   => 'https://www.instagram.com/website.muscle/',
					'title' => 'We\'re on Instagram',
					'icon'  => 'el el-instagram'
				]
			],

			// We can add some footer text that will appear AFTER the option panels
			'footer_text' => '',
		];

		\Redux::setArgs($this->options_name, $this->options);
	}

	/**
	 * Save help tabs
	 * @return void
	 */
	private function initReduxHelp() {
		$tabs = array(
			array(
				'id'      => 'redux-help-tab-1',
				'title'   => __( 'Theme Information 1', 'redux_theme' ),
				'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux_theme' )
			),
			array(
				'id'      => 'redux-help-tab-2',
				'title'   => __( 'Theme Information 2', 'redux_theme' ),
				'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux_theme' )
			)
		);

		\Redux::setHelpTab( $this->options_name, $tabs );

		// Set the help sidebar
		$content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux_theme' );
		\Redux::setHelpSidebar( $this->options_name, $content );
	}

	/**
	 * Adds a section
	 * @param array $args
	 */
	private function setSection( $args = [] ) {
		$this->sections[] = wp_parse_args($args, [
			'id'         => '',
			'title'      => '',
			'subtitle'   => '',
			'desc'       => '',
			'subsection' => false,
			'fields'     => []
		]);
	}

	/**
	 * Adds a section under another
	 * @param $parent
	 * @param $args
	 */
	private function setSubSection($parent, $args) {
		$args['id'] = "{$parent}.{$args['id']}";
		$args['subsection'] = true;

		$this->setSection($args);
	}

	/**
	 * Save sections
	 * @return void
	 */
	private function initReduxSections() {
		$text_domain = $this->get('textdomain');

		// <editor-fold desc="General Settings">
		// ----------------------------------------------------------------------------
		$this->setSection([
			'title'            => __('General Settings', $text_domain),
			'id'               => 'general_settings',
			'icon'             => 'el el-home',
			'customizer_width' => '450px',
			'desc'             => 'Welcome to the Mazloy options panel! You can switch between option groups by using the left-hand tabs.',
		]);
		$this->setSubSection('general_settings', [
			'id'     => 'styling',
			'title'  => __('Styling', $text_domain),
			'fields' => [
				[
					'id'       => 'theme_skin',
					'type'     => 'select',
					'title'    => __( 'Theme Skin', $text_domain ),
					'subtitle' => 'This will alter the overall styling of various theme elements',
					'options'  => [
						// @TODO Adrian Ortega - add more stylesheet options
						'original' => 'Original',
						'dick_sauce' => 'Sauce of the dick'
					],
					'default'  => 'original'
				],
				[
					'id'       => 'favicon',
					'type'     => 'media',
					'title'    => __( 'Faviconon Upload', $text_domain ),

					// @TODO Adrian Ortega - add a sprint for translation
					'subtitle' => 'Upload a 16x16 <code>.png</code> or a <code>.gif</code> that will be your favicon.',
				],
				[
					'id'       => 'button_styling',
					'type'     => 'select',
					'title'    => __( 'Button Styling', $text_domain ),
					'subtitle' => __( 'This will affect the overall styling of buttons' ),
					'options'  => [
						'default'                 => __( 'Default', $text_domain ),
						'slightly_rounded'        => __( 'Slightly Rounded', $text_domain ),
						'slightly_rounded_shadow' => __( 'Slightly Rounded w/ Shadow', $text_domain ),
						'rounded'                 => __( 'Rounded', $text_domain ),
					],
					'default'  => 'default'
				],
				[
					'id'       => 'them_icon_style',
					'type'     => 'select',
					'title'    => __( 'Theme Icon Style', $text_domain ),
					'subtitle' => 'Select your theme icon style here - will be used for menu icons such as shopping cart, search and theme elements such as nectar love, portfolio single navigation etc.',
					'options'  => [
						'inherit' => 'Inherit from Skin',
						'minimal' => 'minimal',
					],
					'default'  => 'minimal'
				],
				[
					'id'          => 'overall_bg_color',
					'type'        => 'color',
					'title'       => __( 'Overall Background Color', $text_domain ),
					'subtitle'    => 'Default is #ffffff',
					'transparent' => false,
					'default'     => '#ffffff'
				],
				[
					'id'          => 'overall_font_color',
					'type'        => 'color',
					'title'       => __( 'Overall Font Color', $text_domain ),
					'subtitle'    => __( 'Default will inherit from skin', $text_domain ),
					'transparent' => false,
					'default'     => ''
				],
				[
					'id'       => 'body_border',
					'type'     => 'switch',
					'title'    => __( 'Body Border (Passepartout)', $text_domain ),
					'subtitle' => __( 'This will add a border around the edges of the screen', $text_domain ),
					'default'  => '0'
				],
				[
					'id'          => 'body_border_color',
					'type'        => 'color',
					'required'    => [ 'body-border', '=', '1' ],
					'title'       => __( 'Body Border Color', $text_domain ),
					'subtitle'    => 'Default is #ffffff',
					'transparent' => false,
					'default'     => '#ffffff'
				],
				[
					'id'       => 'body_border_size',
					'type'     => 'select',
					'required' => [ 'body-border', '=', '1' ],
					'title'    => __( 'Body Border Size', $text_domain ),
					'subtitle' => 'Please choose your desired size in px here. Default is 20px.',
					'options'  => range( 1, 100 ),
					'default'  => 20
				]
			]
		]);
		$this->setSubSection('general_settings', [
			'id'     => 'functionality',
			'title'  => __( 'Functionality', $text_domain ),
			'fields' => [
				[
					'id' => 'loader',
					'type' => 'switch',
					'title' => __('Page Loader', $text_domain),
					'subtitle' => __('Enable the loader animation', $text_domain),
					'default' => '1'
				],
				[
					'id' => 'loader_type',
					'type' => 'select',
					'title' => __('Loader Style', $text_domain),
					'subtitle' => __('Pick what kind of loading animation you\'d prefer', $text_domain),
					'required' => ['loader', '=', '1'],
					'options' => [
						'default' => 'Top Progress',
						'material-bars' => 'Material Bars',
						'material-bars-colors' => 'Material Bars with Material Colors',
						'circle-dot' => 'Circle Dot',
						'circle-notch' => 'Circle Notch',
					],
					'default' => 'default'
				],
				[
					'id' => 'loader_show_text',
					'type' => 'switch',
					'title' => __('Loader Text', $text_domain),
					'subtitle' => __('Enable the loader text', $text_domain),
					'required' => ['loader', '=', '1'],
					'default' => '0'
				],
				[
					'id' => 'loader_text',
					'type' => 'text',
					'title' => __('Loader text', $text_domain),
					'required' => [
						['loader', '=', '1'],
						['loader_show_text', '=', '1'],
					],
					'subtitle' => __('Text to show', $text_domain),
					'default' => 'Loading...'
				],
				[
					'id'       => 'back_to_top',
					'type'     => 'switch',
					'title'    => __( 'Back To Top Button', $text_domain ),
					'subtitle' => __( 'Toggle whether or not to enable a back to top button on your pages.', $text_domain ),
					'default'  => '1'
				],
				[
					'id'       => 'back_to_top_mobile',
					'type'     => 'switch',
					'title'    => __( 'Keep Back To Top Button On Mobile', $text_domain ),
					'subtitle' => __( 'Toggle whether or not to show or hide the back to top button when viewing on a mobile device.', $text_domain ),
					'required' => ['back-to-top', '=', '1'],
					'default'  => '0'
				],
				[
					'id'       => 'smooth_scrolling',
					'type'     => 'switch',
					'title'    => __( 'Styled Scrollbar', $text_domain ),
					'subtitle' => __( 'Toggle whether or not to enable the styled scrollbar - turning this on will lower scrolling performance', $text_domain ),
					'default'  => '0'
				],
				[
					'id'       => 'one_page_scrolling',
					'type'     => 'switch',
					'title'    => __( 'One Page Scroll Support (Animated Anchor Links)', $text_domain ),
					'subtitle' => __( 'Toggle whether or not to enable one page scroll support', $text_domain ),
					'default'  => '1'
				],
				[
					'id'           => 'responsive',
					'type'         => 'switch',
					'title'        => __( 'Enable Responsive Design', $text_domain ),
					'subtitle'     => __( 'This adjusts the layout of your website depending on the screen size/device.', $text_domain ),
					'next_to_hide' => '1',
					'default'      => '1'
				],
				[
					'id'       => 'ext_responsive',
					'type'     => 'switch',
					'required' => [ 'responsive', '=', '1' ],
					'title'    => __( 'Extended Responsive Design', $text_domain ),
					'subtitle' => __( 'This will enhance the way the theme responds when viewing on screens larger than 1000px & increase the max width.', $text_domain ),
					'default'  => '1'
				],
				[
					'id'            => 'max_container_width',
					'type'          => 'slider',
					'required'      => [ 'ext_responsive', '=', '1' ],
					'title'         => __( 'Max Website Container Width', $text_domain ),
					'subtitle'      => __( 'When using the extended responsive design your container will scale to a maximum width of 1425px, use this option if you\'d like to increase that value.', $text_domain ),
					"default"       => 1425,
					"min"           => 1425,
					"step"          => 1,
					"max"           => 2000,
					'display_value' => 'text'
				]
			]
		]);
		$this->setSubSection('general_settings', [
			'id'     => 'theme-features',
			'title'  => __( 'Toggle Theme Features', $text_domain ),
			'fields' => [
				[
					'id'       => 'disable_tgm',
					'type'     => 'switch',
					'title'    => __( 'Disable Theme Recommended Plugin Notifications', $text_domain ),
					'subtitle' => __( 'This will remove the notifications shown for installing/updating recomended theme plugins. Enable if you don\'t need them anymore & are familar with keeping track of plugin updates in WordPress. <b>Will yield Admin panel performance improvement</b>', $text_domain ),
					'default'  => '0'
				],

				// @TODO Adrian Ortega - Come up with a way better way of doing this... thanks
				[
					'id'       => 'disable_home_slider_pt',
					'type'     => 'switch',
					'title'    => __( 'Disable Home Slider', $text_domain ),
					'subtitle' => __( 'This will remove the Home Slider post type <b>Will yield Admin panel & front-end performance improvement</b>', $text_domain ),
					'desc'     => '',
					'default'  => '0'
				]
			]
		]);
		//</editor-fold>\

		// <editor-fold desc="Theme Colors">
		// ----------------------------------------------------------------------------
		$this->setSection([
			'id'               => 'accent_color',
			'customizer_width' => '450px',
			'icon'             => 'el el-brush',
			'title'            => __( 'Accent Colors', $text_domain ),
			'desc'             => __( 'All accent color related options are listed here', $text_domain ),
			'fields'           => [
				[
					'id'          => 'accent_color',
					'type'        => 'color',
					'transparent' => false,
					'title'       => __( 'Accent Color', $text_domain ),
					'subtitle'    => __( 'Change this color to alter the accent color globally for your site.', $text_domain ),
					'desc'        => '',
					'default'     => '#3452ff'
				],
				[
					'id'          => 'extra_color_1',
					'type'        => 'color',
					'transparent' => false,
					'title'       => __( 'Extra Color #1', $text_domain ),
					'subtitle'    => __( 'Applicable theme elements will have the option to choose this as a color <br/> (i.e. buttons, icons etc..)', $text_domain ),
					'desc'        => '',
					'default'     => '#ff1053'
				],
				[
					'id'          => 'extra_color_2',
					'type'        => 'color',
					'transparent' => false,
					'title'       => __( 'Extra Color #2', $text_domain ),
					'subtitle'    => __( 'Applicable theme elements will have the option to choose this as a color <br/> (i.e. buttons, icons etc..)', $text_domain ),
					'desc'        => '',
					'default'     => '#2AC4EA'
				],
				[
					'id'          => 'extra_color_3',
					'type'        => 'color',
					'transparent' => false,
					'title'       => __( 'Extra Color #3', $text_domain ),
					'subtitle'    => __( 'Applicable theme elements will have the option to choose this as a color <br/> (i.e. buttons, icons etc..)', $text_domain ),
					'desc'        => '',
					'default'     => '#333333'
				],
				[
					'id'          => 'extra_color_gradient',
					'type'        => 'color_gradient',
					'transparent' => false,
					'title'       => __( 'Extra Color Gradient', $text_domain ),
					'subtitle'    => __( 'Applicable theme elements will have the option to choose this as a color <br/> (i.e. buttons, icons etc..)', $text_domain ),
					'desc'        => '',
					'default'     => [
						'from' => '#3452ff',
						'to'   => '#ff1053'
					],
				],
				[
					'id'          => 'extra_color_gradient_2',
					'type'        => 'color_gradient',
					'transparent' => false,
					'title'       => __( 'Extra Color Gradient #2', $text_domain ),
					'subtitle'    => __( 'Applicable theme elements will have the option to choose this as a color <br/> (i.e. buttons, icons etc..)', $text_domain ),
					'desc'        => '',
					'default'     => [
						'from' => '#2AC4EA',
						'to'   => '#32d6ff'
					],
				]
			]
		]);
		// </editor-fold>

		// <editor-fold desc="Layout">
		// ----------------------------------------------------------------------------
		$this->setSection([
			'id' => 'boxed_layout',
			'customizer_width' => '450px',
			'icon' => 'el el-website',
			'title' => __('Boxed Layout', $text_domain),
			'desc' => __('All boxed layout related options are listed here', $text_domain),
			'fields' => [
				[
					'id' => 'boxed_layout',
					'type' => 'switch',
					'title' => __('Enable Boxed Layout?', $text_domain),
					'subtitle' => __('', $text_domain),
					'next_to_hide' => '6',
					'default' => '0'
				], [
					'id' => 'background_color',
					'type' => 'color',
					'title' => __('Background Color', $text_domain),
					'subtitle' => __('If you would rather simply use a solid color for your background, select one here.', $text_domain),
					'transparent' => false,
					'required' => ['boxed_layout', '=', '1'],
					'default' => '#f1f1f1'
				], [
					'id' => 'background_image',
					'type' => 'media',
					'title' => __('Background Image', $text_domain),
					'subtitle' => __('Upload your background here', $text_domain),
					'required' => ['boxed_layout', '=', '1'],
				], [
					'id' => 'background_repeat',
					'type' => 'switch',
					'title' => __('Background Repeat', $text_domain),
					'subtitle' => __('Do you want your background to repeat? (Turn on when using patterns)', $text_domain),
					'required' => ['boxed_layout', '=', '1'],
					'default' => '0'
				], [
					'id' => 'background_position',
					'type' => 'select',
					'title' => __('Background Position', $text_domain),
					'subtitle' => __('How would you like your background image to be aligned?', $text_domain),
					'required' => ['boxed_layout', '=', '1'],
					'options' => [
						"left top" => "Left Top",
						"left center" => "Left Center",
						"left bottom" => "Left Bottom",
						"center top" => "Center Top",
						"center center" => "Center Center",
						"center bottom" => "Center Bottom",
						"right top" => "Right Top",
						"right center" => "Right Center",
						"right bottom" => "Right Bottom"
					]
				], [
					'id' => 'background_attachment',
					'type' => 'radio',
					'title' => __('Background Attachment', $text_domain),
					'subtitle' => __('Would you prefer your background to scroll with your site or be fixed and not move', $text_domain),
					'required' => ['boxed_layout', '=', '1'],
					'options' => [
						"scroll" => "Scroll",
						"fixed" => "Fixed"
					]
				], [
					'id' => 'background_cover',
					'type' => 'switch',
					'title' => __('Auto resize background image to fit window?', $text_domain),
					'subtitle' => __('This will ensure your background image always fits no matter what size screen the user has. (Don\'t use with patterns)', $text_domain),
					'required' => ['boxed_layout', '=', '1' ],
					'default' => '0'
				]
			]
		]);
		// </editor-fold>

		// <editor-fold desc="Typography">
		// ----------------------------------------------------------------------------
		$this->setSection([
			'id' => 'typography',
			'title' => __('Typography', $text_domain),
			'desc' => __('All typography related options are listed here', $text_domain),
			'icon' => 'el el-font'
		]);
		$this->setSubSection('typography', [
			'id'     => 'navigation',
			'title'  => __( 'Navigation & Page Header', $text_domain ),
			'desc'   => 'Set font properties for Navigation and Page Header options',
			'fields' => [
				General::fontFamily(
					'navigation_font_family',
					__( 'Navigation Font', $text_domain ),
					__( 'Specify the Navigation font properties.', $text_domain )
				),
				General::fontFamily(
					'navigation_dropdown_font_family',
					__( 'Navigation Dropdown Font', $text_domain ),
					__( 'Specify the Navigation Dropdown font properties', $text_domain )
				),
				General::fontFamily(
					'page_heading_font_family',
					__( 'Page Heading Font', $text_domain ),
					__( 'Specify the Page Heading font properties.', $text_domain )
				),
				General::fontFamily(
					'page_heading_subtitle_font_family',
					__( 'Page Heading Subtitle Font', $text_domain ),
					__( 'Specify the Page Heading Subtitle font properties.', $text_domain )
				),
				General::fontFamily(
					'off_canvas_nav_font_family',
					__( 'Off Canvas Navigation', $text_domain ),
					__( 'Specify the Off Canvas Navigation properties.', $text_domain )
				),
				General::fontFamily(
					'off_canvas_nav_subtext_font_family',
					__( 'Off Canvas Navigation Sub Text', $text_domain ),
					__( 'Specify the Off Canvas Navigation Sub Text properties.', $text_domain )
				)
			]
		]);
		$this->setSubSection('typography', [
			'id'     => 'general',
			'title'  => __( 'General HTML Elements', $text_domain ),
			'fields' => array_merge( [
				General::fontFamily(
					'body_font_family',
					__( 'Body Font', $text_domain ),
					__( 'Specify the Body font properties.', $text_domain )
				),
			], array_map( function ( $h ) use ( $text_domain ) {
				return General::fontFamily(
					"h{$h}_font_family",
					sprintf( __( 'Heading %d', $text_domain ), $h ),
					sprintf( __( 'Specify the H%d Text properties.', $text_domain ), $h )
				);
			}, range( 1, 6 ) ), [
				General::fontFamily(
					'i_font_family',
					__( 'Italic', $text_domain ),
					__( 'Specify the italic text properties', $text_domain )
				),
				General::fontFamily(
					'label_font_family',
					__( 'Form Labels', $text_domain ),
					__( 'Specify the Form Label properties', $text_domain )
				)
			] )
		] );
		// </editor-fold>

		// <editor-fold desc="Header Navigation">
		// ----------------------------------------------------------------------------
		$this->setSection([
			'id' => 'header_nav',
			'title' => __('Header Navigation', $text_domain),
			'desc' => __('All header navigation related options are listed here', $text_domain),
			'icon' => 'el el-lines',
		]);
		$this->setSubSection('header_nav', [
			'id' => 'general',
			'title' => __('Logo & General Styling', $text_domain),
			'fields' => [
				[
					'id' => 'use_logo',
					'type' => 'switch',
					'title' => __('Use Image for Logo', $text_domain),
					'subtitle' => __('If left unchecked, plain text will be used instead of an image', $text_domain),
				],
				[
					'id' => 'logo',
					'type' => 'media',
					'title' => __('Logo Upload', $text_domain),
					'required' => ['use_logo', '=', '1'],
					'subtitle' => __('Upload your logo here and enter the height of it below', $text_domain)
				],
				[
					'id' => 'retina_logo',
					'type' => 'media',
					'title' => __('Retina Logo Upload', $text_domain),
					'required' => ['use_logo', '=', '1'],
					'subtitle' => __('Upload at exactly 2x the size of your standard logo.')
				],
				[
					'id' => 'logo_height',
					'type' => 'text',
					'title' => __('Logo Height', $text_domain),
					'required' => ['use_logo', '=', '1'],
					'validate' => 'number'
				],
				[
					'id' => 'mobile_logo_height',
					'type' => 'text',
					'title' => __('Mobile Logo Height', $text_domain),
					'required' => ['use_logo', '=', '1'],
					'validate' => 'number',
				],
				[
					'id' => 'header_padding',
					'type' => 'text',
					'title' => __('Header Padding', $text_domain),
					'validate' => 'number'
				],
				[
					'id' => 'header_mobile_fixed',
					'type' => 'switch',
					'title' => __('Header sticky on Mobile', $text_domain),
					'subtitle' => __('Do you want the header to be sticky on mobile devices', $text_domain),
					'default' => '1'
				],
				[
					'id' => 'header_utility_menu_enable',
					'type' => 'switch',
					'title' => __('Header Utility Menu', $text_domain),
					'subtitle' => __('Enable the utility menu above the website', $text_domain),
					'default' => '1'
				]
			]
		]);
		// </editor-fold>

		// <editor-fold desc="Header Navigation">
		// ----------------------------------------------------------------------------
		$this->setSection([
			'id' => 'blog',
			'title' => __('Blog', $text_domain),
			'desc' => __('All of the settings pertaining to the blog', $text_domain),
			'icon' => 'el el-pencil',
		]);
		$this->setSubSection('blog', [
			'id' => 'author',
			'title' => __('Author', $text_domain),
			'fields' => [
				[
					'id' => 'blog_show_author',
					'type' => 'switch',
					'title' => __('Show Author Block After Posts', $text_domain),
					'subtitle' => __('Control if the author should show or not and toggle other options', $text_domain),
					'default' => '1'
				],
				[
					'id' => 'blog_author_avatar_size',
					'type' => 'slider',
					'title' => __('Author Block Avatar Size', $text_domain),
					'subtitle' => __('How big would you like the avatar to be in the Author Block (this is in pixels).', $text_domain),
					'default' => 100,
					'min' => 32,
					'max' => 256,
					'step' => 1,
					'required' => ['show_author', '=', '1']
				],
				[
					'id' => 'blog_author_title',
					'type' => 'switch',
					'title' => __('Show Author Block Title', $text_domain),
					'subtitle' => __('Display a title above the author block', $text_domain),
					'default' => '0'
				],
				[
					'id' => 'blog_author_section_title',
					'type' => 'text',
					'title' => __('Block Title', $text_domain),
					'default' => __('More about the author', $text_domain),
					'required' => ['blog_author_title', '=', '1']
				]
			]
		]);
		$this->setSubSection('blog', [
			'id' => 'related_posts',
			'title' => __('Related Posts', $text_domain),
			'fields' => [
				[
					'id' => 'blog_show_related',
					'type' => 'switch',
					'title' => __('Show related Posts', $text_domain),
					'subtitle' => __('Show related posts under the author', $text_domain),
					'default' => '1'
				],
				[
					'id' => 'blog_related_section_title',
					'type' => 'text',
					'title' => __('Block Title', $text_domain),
					'default' => __('Related Posts'),
					'required' => ['blog_show_related', '=', '1']
				],
				[
					'id' => 'blog_related_count',
					'type' => 'slider',
					'title' => __('How many posts', $text_domain),
					'subtitle' => __('How many related posts should show up', $text_domain),
					'default' => '3',
					'min' => '1',
					'max' => '12',
					'step' => '1',
					'required' => ['blog_show_related', '=', '1']
				]
			]
		]);
		//  </editor-food>

		// <editor-fold desc="Socials">
		$_social_fields = [];
		foreach(mazloy_get_social_network_defaults() as $id => $default) {
			$id_prefix = sprintf('social_%s', $id);
			$enable_id = sprintf('%s_enable', $id_prefix);
			$_social_fields[] = [
				'id' => $enable_id,
				'title' => $default[0],
				'subtitle' => sprintf(__('Show %s link', $text_domain), $default[0]),
				'type' => 'switch',
				'default' => isset($default[2]) ? $default[1] : '1',
			];
			$_social_fields[] = [
				'id' => sprintf('%s_url', $id),
				'title' => sprintf(__('%s Page URL', $text_domain), $default[0]),
				'type' => 'text',
				'required' => [$enable_id, '=', '1'],
				'validate' => 'url'
			];
			$_social_fields[] = [
				'id' => sprintf('%s_alt', $id_prefix),
				'type' => 'text',
				'title' => sprintf(__('%s button alt text', $text_domain), $default[0]),
				'subtitle' => __('Modify the <code>alt</code> text that shows up when hovering over the icon link'),
				'required' => [$enable_id, '=', '1'],
				'default' => isset($default[1]) ? $default[1] : ''
			];
		}

		$this->setSection([
			'id' => 'social',
			'title' => __('Social Networks', $text_domain),
			'customizer-width' => '450px',
			'icon' => 'el el-thumbs-up',
			'desc' => __('Add your social media links'),
			'fields' => $_social_fields
		]);
		$this->setSubSection('social', [
			'id' => 'header_social',
			'title' => __('Header Bar', $text_domain),
			'fields' => [
				[
					'id' => 'social_header_enable',
					'title' => __('Show Header Links', $text_domain),
					'subtitle' => __('Show the global social networks header bar'),
					'type' => 'switch',
					'default' => '1'
				]
			]
		]);
		// </editor-fold>

		foreach($this->sections as $section) {
			Redux::setSection($this->options_name, $section);
		}
	}

	/**
	 * Run
	 */
	public function run() {
		$this->initReduxOptions();
		$this->initReduxHelp();
		$this->initReduxSections();

		$this->loader()->addAction( 'admin_head', [ $this, 'adminHead' ] );
		$this->loader()->addFilter( 'redux/options/' . $this->options_name . '/compiler', [ $this, 'compiler' ], 10, 3 );
	}
}