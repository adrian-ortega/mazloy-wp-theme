<?php

namespace Mazloy\Metaboxes;

use Mazloy\Core\QuickMetabox;

class PageRowsMetabox extends QuickMetabox
{
	public function setup() {
		$text_domain = $this->get('textdomain');

		return $this->boot([
			'name'      => __( 'Page Full Screen Options', $text_domain ),
			'metakey'   => 'wm_page_rows',
			'post_type' => 'page',
			'context'   => 'advanced',
			'fields' => [
				'__description' => [
					'type' => 'note',
					'label' => '',
					'description' => 'Here you can configure how rows function within <strong>this</strong> page',
				],
				'full_screen' => [
					'type' => 'switch',
					'label' => __('Activate Fullscreen Rows', $text_domain),
					'sub_label' => __('This will cause all rows to be fullscreen. Some functionality and options within visual composer will be changed when this is active', $text_domain),
					'default' => 0,
				],
				'animation' => [
					'type' => 'select',
					'label' => __('Animation Between Rows', $text_domain),
					'sub_label' => __('Select your desired animation speed', $text_domain),
					'options' => [
						'default' => 'Default',
						'zoom_scroll' => 'Zoom Out + Parallax',
						'parallax' => 'Parallax'
					],
					'default_value' => 'default',
					'required' => ['full_screen', '=', 1]
				],
				'animation_speed' => [
					'type' => 'select',
					'label' => __('Animation Speed', $text_domain),
					'sub_label' => __('Selection your desired animation speed', $text_domain),
					'options' => [
						'slow' => __('Slow', $text_domain),
						'medium' => __('Medium', $text_domain),
						'fast' => __('Fast', $text_domain)
					],
					'default_Value' => 'medium',
					'required' => ['full_screen', '=', 1]
				],
				'anchors' => [
					'type' => 'switch',
					'label' => __('Add Row Anchors to URL', $text_domain),
					'sub_label' => __('Enable this to add anchors into your URL for each row'),
					'default' => 0,
					'required' => ['full_screen', '=', 1]
				],

				// Save!
				'__save_button' => [
					'type' => 'update_button',
					'required' => ['full_screen', '=', 1]
				],
				'__console_logger' => [
					'type' => 'html',
					'html' => '<div class="field field-console"><a href="#" onclick="WMTheme.metaboxLogger(this, \'wm_page_rows\', event)">Console Log Data</a></div>',
					'required' => ['activate', '=', 1],
				]
			]
		]);
	}
}