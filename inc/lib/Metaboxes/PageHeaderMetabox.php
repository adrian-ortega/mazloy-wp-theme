<?php

namespace Mazloy\Metaboxes;

use Mazloy\Core\QuickMetabox;

class PageHeaderMetabox extends QuickMetabox
{
	public function setup() {
		$text_domain = $this->get( 'textdomain' );
		return $this->boot([
			'name'      => __( 'Page Header Options', $text_domain ),
			'metakey'   => 'wm_page_header',
			'post_type' => 'page',
			'context'   => 'advanced',
			'fields' => [
				'__note' => [
					'type' => 'note',
					'label' => '',
					'description' => 'Here you can configure how your page header will appear. For a full width background image behind your header text, simply upload the image below.',
				],

				'background_type' => [
					'type' => 'button_option',
					'label' => __('Background Type', $text_domain),
					'sub_label' => __('Please select the background type you would like to use for your slide.', $text_domain),
					'options' => [
						'image' => __('Image Background'),
						'video' => __('Video Background'),
						//'html5' => __('HTML5 Canvas Background')
					]
				],
				'background_image' => [
					'type' => 'image',
					'label' => __('Background Image', $text_domain),
					'sub_label' => __('Pick the image you\'d like to use as the background image', $text_domain),
					'required' => ['background_type', '=', 'image']
				],
				'scroll_effect' => [
					'type' => 'select',
					'label' => __('Scroll Effect', $text_domain),
					'sub_label' => __('Choose your desired scroll effect here', $text_domain),
					'options' => [
						'none' => __('None', $text_domain),
						'parallax' => __('Parallax', $text_domain),
					],
					'default' => 'none'
				],
				'header_height' => [
					'type' => 'number',
					'label' => __('Page Header Height', $text_domain),
					'sub_label' => __('How tall do you want your header? Don\'t include the "px" in the string, e.g. 350. This only applies to images', $text_domain),
				],
				'header_align' => [
					'type' => 'element_position',
					'label' => __('Header Content Alignment'),
					'sub_label' => __('Configure the position for the page header content', $text_domain),
					'default' => [
						'horizontal' => 'center',
						'vertical' => 'center'
					]
				],
				'header_font_color' => [
					'type' => 'color',
					'label' => __('Page Header Font Color', $text_domain),
					'sub_label' => __('Set your desired page header font color', $text_domain)
				],
				'header_overlay_color' => [
					'type' => 'color',
					'label' => __('Page Header Overlay Color'),
					'sub_label' => __('This will be applied ontop on your page header BG Image, if supplied', $text_domain)
				],
				'header_overlay_opacity' => [
					'type' => 'range',
					'label' => __('Page Header Overlay Opacity'),
					'min' => 0,
					'max' => 100,
					'default' => 10
				],

				// Save!
				'__save_button' => [
					'type' => 'update_button',
					'required' => ['activate', '=', 1]
				],
				'__console_logger' => [
					'type' => 'html',
					'html' => '<div class="field field-console"><a href="#" onclick="WMTheme.metaboxLogger(this, \'wm_page_rows\', event)">Console Log Data</a></div>',
					'required' => ['activate', '=', 1],
				]
			]
		]);
	}
}