<?php

/**
 * Returns an option value
 *
 * @param string $path
 *
 * @param null $default
 *
 * @return mixed
 */
function mazloy_get_option($path, $default = null) {
	// ugh, globals?
	global $website_muscle_options;

	return isset($website_muscle_options[$path]) ? $website_muscle_options[$path] : $default;
}

/**
 * Checks to see if an option is turned on. This is mostly to be used with switches
 * @param $path
 *
 * @return bool
 */
function mazloy_enabled($path) {
	$option = mazloy_get_option($path);
	return $option && $option === '1';
}

/**
 * Print an option value
 * @param string $path
 */
function mazloy_option($path) {
	echo mazloy_get_option($path);
}

/**
 * Returns the page template being used by a particular page
 * @param int|null $post_id
 * @return array
 */
function mazloy_get_page_templates($post_id = null) {
	if(!$post_id)
		$post_id = get_the_ID();
	$raw = get_post_meta( $post_id, '_wp_page_template', true );
	$named = '';

	foreach(wp_get_theme()->get_page_templates(null, 'page') as $path => $name) {
		if($path == $raw) {
			$named = $name;
			break;
		}
	}

	$templates = [];
	if(!empty($raw)) {
		$templates[] = $raw;
		if(!empty($named))
			$templates[] = $named;
		$templates[] = sanitize_title(str_replace([
			'.php',
			'page',
			'templates/',
			'template-',
			'template',
		], '', $raw ));
	}
	return $templates;
}

/**
 * Checks to see if we're using a specific page template
 * @param string|array $slugs
 * @param null|int $post_id
 * @return bool
 */
function mazloy_is_page_template($slugs, $post_id = null) {
	$slugs = (array) $slugs;
	$templates = mazloy_get_page_templates($post_id);
	$found = false;
	foreach($slugs as $slug) {
		if(in_array($slug, $templates)) {
			$found = true;
			break;
		}
	}
	return $found;
}

/**
 * Creates a quick metabox using args
 * @param string $name
 * @param null|string|array $args
 * @return \Mazloy\Core\QuickMetabox
 */
function mazloy_metabox($name = null, $args = null) {

	if(is_array($name)) {
		$args = $name;
		$name = isset($args['name']) ? $args['name'] : null;
	}

	$container = mazloy()->container();
	$box = null;

	// Check for an array, this means we want a quick box.
	if(is_array($args)) {
		$box = new \Mazloy\Core\QuickMetabox( $container );
		$box->boot($args);
	}

	// if a fully qualified class name is passed, a more complex box
	// needs to be spun up
	elseif(class_exists($args)) {
		$box = new $args( $container );
	}

	return $box instanceof \Mazloy\Core\Abstracts\MetaboxAbstract ? $box->enqueue( $name ) : null;
}

/**
 * @param string $metabox
 * @param string $key
 * @param null|int|WP_Post $post
 * @return null|mixed
 */
function mazloy_metabox_value($metabox = '', $key = null, $post = null) {
	if(empty($post) || is_int($post))
		$post = get_post($post);

	if(!($box = mazloy("quick_metabox_{$metabox}")))
		$box = mazloy("metabox_{$metabox}");

	// if we're not inside a loop, no reason to continue.
	// or if the box does not exist within the registered boxes.
	return !$post || !$box ? null : $box->get_meta($key, $post);
}

/**
 * Checks to see if a string contains one or multiple strings
 * @param string $haystack
 * @param string|array $needles
 * @param int $offset
 * @return bool
 */
function mazloy_str_contains($haystack, $needles, $offset = 0) {
	foreach((array) $needles as $needle)
		if($position = strpos($haystack, $needle, $offset) !== false)
			return true;

	return false;
}

function mazloy_get_html_attrs($attributes = []) {
	return \Mazloy\Core\Markup::attributes($attributes);
}

function mazloy_html_attrs($attributes = []) {
	return mazloy_get_html_attrs($attributes);
}

function mazloy_get_social_network_defaults() {
	$text_domain = mazloy('textdomain');
	return [
		'facebook'  => [ __( 'Facebook', $text_domain ), __( 'Like us on Facebook', $text_domain ), '0' ],
		'twitter'   => [ __( 'Twitter', $text_domain ), __( 'Follow us on Twitter', $text_domain ), '0' ],
		'instagram' => [ __( 'Instagram', $text_domain ), __( 'Check us out on Instagram', $text_domain ), '0' ],
		'linkedin'  => [ __( 'Linked In', $text_domain ), __( 'Find us on LinkedIn', $text_domain ), '0' ],
		'youtube'   => [ __( 'YouTube', $text_domain ), __( 'Watch us on YouTube', $text_domain ), '0' ],
		'vimeo'     => [ __( 'Vimeo', $text_domain ), __( 'Watch us on Vimeo', $text_domain ), '0' ],
	];
}

function mazloy_get_social_networks() {
	$networks = [];
	foreach(mazloy_get_social_network_defaults() as $id => $item) {
		$id_prefix = sprintf('social_%s', $id);
		if(mazloy_enabled($id_prefix . '_enable')) {
			$network = new stdClass();
			$network->id = $id;
			$network->alt_text = mazloy_get_option( $id_prefix . '_alt' );
			$network->url = mazloy_get_option( $id_prefix . '_url' );
			$network->icon = sprintf( '<span class="mdi mdi-%s"></span>', $id == 'youtube' ? 'youtube-play' : $id );
			$network->name = $item[0];
			$networks[] = $network;
		}
	}
	return $networks;
}

/**
 * @param null $post_type
 * @return array|\Mazloy\Assets\Slide[]
 */
function mazloy_get_slides($post_type = null) {
	$slides = [];

	if(!empty($post_type)) {
		$query = new WP_Query( [
			'post_type'      => $post_type,
			'posts_per_page' => - 1,
			'post_status'    => 'publish'
		] );

		while($query->have_posts()) {
			$query->the_post();
			$slides[] = new \Mazloy\Assets\Slide($query->post);
		}

		wp_reset_query();
	}
	return $slides;
}